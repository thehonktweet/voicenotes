class GUI_Element{
  
  GUI_Element _parent_element = null;
  GUI_Element _sensing_area = null;
  ArrayList<GUI_Element> _children_list = new ArrayList<GUI_Element>();

  String _name = "gui_element";

  boolean _is_interactive = false;
  boolean _is_active = false;
  boolean _is_visible = true;
  String _view_mode = "both"; //list_view, map_view
  String _type = ""; //ex: is the gui element a voice note? what are the types of elements?
  
  int _x = 0; //location of top left corner
  int _y = 0;
  int _width = 0;
  int _height = 0;

  int _scroll_y = 0;

  int _position_offset_x = 0;
  int _position_offset_y = 0;
  
  ArrayList<Attribute> _attribute_list = new ArrayList<Attribute>();
  Folder _folder = null;
  Note _note = null;

  GUI_Element(Folder a_folder)
  {
    _view_mode = "list_view";
    _folder = a_folder;
    _type = "folder";
    _is_interactive = true;
    _is_active = false;
  }

  GUI_Element(Note note)
  {
    _view_mode = "list_view";
    _note = note;
    _type = "note";
    _is_interactive = true;
    _is_active = false;
  }

  GUI_Element(String type, String view_mode, int x, int y, int w, int h)
  {
    _x = x;
    _y = y;
    _width = w;
    _height = h;
    _type = type;
    _view_mode = view_mode;
  }

  GUI_Element(String type, String view_mode, boolean is_interactive, int x, int y, int w, int h)
  {
    _x = x;
    _y = y;
    _width = w;
    _height = h;
    _type = type;
    _view_mode = view_mode;
    _is_interactive = is_interactive;
  }

  GUI_Element(String type, String view_mode)
  {
    _type = type;
    _view_mode = view_mode;
  }

  void setName(String name)
  {
    _name = name;
  }

  String getName()
  {
    return _name;
  }

  void setSensingArea(GUI_Element sensing_area)
  {
    _sensing_area = sensing_area;
  }

  GUI_Element getSensingArea()
  {
    return _sensing_area;
  }

  void setInteractiveStatus(boolean i)
  {
    _is_interactive = i;
  }

  void setActiveStatus(boolean a)
  {
    _is_active = a;
    //setChildrenStatus(a);
  }

  boolean getActiveStatus()
  {
    return _is_active;
  }

  void setVisibleStatus(boolean status)
  {
    _is_visible = status;
  }

  boolean isVisible()
  {
    return _is_visible;
  }

  boolean toggleActiveStatus()
  {
    if(_is_active == false)
    {
      _is_active = true;
      setChildrenStatus(true);
    }
    else
    {
      _is_active = false;
      setChildrenStatus(false);
    }

    return _is_active;
  }

  void setChildrenStatus(boolean status)
  {
    for(int i = 0; i < _children_list.size(); i++)
    {
      if(_type == "folder" && _children_list.get(i).getType() == "note")
      {
        //do nothing
      }
      else 
      {
        _children_list.get(i).setActiveStatus(status);
      }
      
    }
  }
  void addChildren(GUI_Element child)
  {
    _children_list.add(child);
    setChildrenStatus(_is_active);
  }

  ArrayList<GUI_Element> getChildren()
  {
    return _children_list;
  }

  boolean isInteractive()
  {
    return _is_interactive;
  }

  void calculateAbsolutePosition()
  {
    //maybe there is a way to filter attributes by "position" type?

    //Default values 
    int x_offset = 10;
    int y_offset = 10;

    int parent_x = _parent_element.getX();
    int parent_y = _parent_element.getY();

    int parent_width = _parent_element.getWidth();
    int parent_height = _parent_element.getHeight();

    float parent_scale_x = 0.5;
    float parent_scale_y = 0.5;

    String horizontal_alignment = "left";
    String vertical_alignment = "top";
    int stroke_weight = 1;

    //now gather the actual values
    for(int i = 0; i<_attribute_list.size(); i++)
    {
      Attribute att = _attribute_list.get(i);
      String att_name = att.getName();
      String att_value = att.getValue().toString();
       
      switch(att_name){
        case "parent_scale_x":
          parent_scale_x = float(att_value);
          break;

        case "parent_scale_y":
          parent_scale_y = float(att_value);
          break;

        case "parent_align_x":
          horizontal_alignment = att_value;
          break;

        case "parent_align_y":
          vertical_alignment = att_value;
          break;

        case "parent_offset_x":
          x_offset = int(att_value);
          break;

        case "parent_offset_y":
          y_offset = int(att_value);
          break;

        case "stroke_weight":
          stroke_weight = int(att_value);
          break;
      }
    }

    //now do your calculatios!
    _width = int(parent_width * parent_scale_x);
    _height = int(parent_height * parent_scale_y);

    if(horizontal_alignment == "center")
    {
      _x = int( ((parent_width - _width)*0.5) + parent_x );
    }
    else if(horizontal_alignment == "left")
    {
      _x = parent_x;
    }
    else
    {
      _x = ((parent_x + parent_width) - _width) ;
    }

    if(vertical_alignment == "center")
    {
      _y = int( ((parent_height - _height)*0.5) + parent_y );
    }
    else if(vertical_alignment == "top")
    {
      _y = parent_y;
    }
    else
    {
      _y = ((parent_y + parent_height) - _height);
    }    
    
    _x += x_offset;
    _y += y_offset;
  }

  void setPositionOffset(int x, int y)
  {
    _position_offset_x = x;
    _position_offset_y = y;
  }

  void setChildrenPositionOffset(int x, int y)
  {
    if(_type == "folder" || _type == "note")
    {
      _sensing_area.setPositionOffset(x,y);
    }
    for(int i = 0; i < _children_list.size(); i++)
    {
      if(_children_list.get(i).getType() == "note")
      {
        _children_list.get(i).setChildrenPositionOffset(x,y);
      }
      else 
      {
        _children_list.get(i).setPositionOffset(x,y);
      }
      
    }
  }

  void setChildrenScrollY(int y)
  {
    if(_type == "folder" || _type == "note")
    {
      _sensing_area.setScrollY(y);
    }
    for(int i = 0; i < _children_list.size(); i++)
    {
      if(_children_list.get(i).getType() == "note")
      {
        _children_list.get(i).setChildrenScrollY(y);
      }
      else 
      {
        _children_list.get(i).setScrollY(y);
      }
      
    }
  }

  void setParentElement(GUI_Element parent)
  {
    _parent_element = parent;
  }
  
  void addAttribute(Attribute an_attribute)
  {
    _attribute_list.add(an_attribute);
  }

  void setWidth(int w)
  {
    _width = w;
  }

  void setHeight(int h)
  {
    _height = h;
  }
  void setPositionOffsetX(int x)
  {
    _position_offset_x = x;
  }
  void setPositionOffsetY(int y)
  {
    _position_offset_y = y;
  }

  void setScrollY(int y)
  {
    _scroll_y = y;
  }

  void setX(int x)
  {
    _x = x;
  }
  void setY(int y)
  {
    _y = y;
  }
  GUI_Element getParent()
  {
    return _parent_element;
  }
  int getPositionOffsetX()
  {
    return _position_offset_x;
  }

  int getPositionOffsetY()
  {
    return _position_offset_y;
  }

  int getCurrentX()
  {
    return _x + _position_offset_x;
  }

  int getCurrentY()
  {
    return _y + _position_offset_y + _scroll_y;
  }

  Note getNote()
  {
    return _note;
  }
  String getViewMode()
  {
    return _view_mode;
  }
  
  String getType()
  {
    return _type;
  }
  
  int getX()
  {
    return _x;
  }
  
  int getY()
  {
    return _y;
  }
  
  int getWidth()
  {
    return _width;
  }
  
  int getHeight()
  {
    return _height;
  }
  
  ArrayList<Attribute> getAllAttributeList()
  {
    return _attribute_list;
  }

  ArrayList<Attribute> getAttributeList()
  {
    if(_is_interactive)
    {
      if(_is_active) return getActiveAttributes();
      else return getInactiveAttributes();
    }
    else return getAllAttributeList();
    
  }

  ArrayList<Attribute> getActiveAttributes()
  {
    ArrayList<Attribute> active_attributes = new ArrayList<Attribute>();
    for(int i = 0; i < _attribute_list.size(); i++)
    {
      Attribute att = _attribute_list.get(i);
      if(att.getType() == "active" || att.getType() == "both")
      {
        active_attributes.add(att);
      }
    }
    
    return active_attributes;
  }

  ArrayList<Attribute> getInactiveAttributes()
  {
    ArrayList<Attribute> inactive_attributes = new ArrayList<Attribute>();
    for(int i = 0; i < _attribute_list.size(); i++)
    {
      Attribute att = _attribute_list.get(i);
      if(att.getType() == "inactive" || att.getType() == "both")
      {
        inactive_attributes.add(att);
      }
    }
    
    return inactive_attributes;
  }
  
  /*
  ArrayList<GUI_Element> getSubElements()
  {
    return _sub_elements_array;
  }*/
}
