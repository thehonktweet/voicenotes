class ScrollRect {
 
  float rectPosX=0;
  float rectPosY=0;
  float rectWidth=14; 
  float rectHeight=30;
  
  float _var_x = 0;
  float _var_y = 0;

  float _var_height = 100;

  float max_val = 1000;
 
  boolean holdScrollRect=false; 
 
  float offsetMouseY; 
  boolean _is_visible = true;
 
  //constr
  ScrollRect(int var_x, int var_y, int var_height) {

    // you have to make a scrollRect in setup after size()
    rectPosX= var_x;
    rectPosY = var_y;
    _var_x = var_x;
    _var_y = var_y;
    _var_height = var_height;
  }//constr

  void setVisibleStatus(boolean status)
  {
    _is_visible = status;
  }

  boolean getVisibleStatus()
  {
    return _is_visible;
  }
 
  void setWidth(int w)
  {
    rectWidth = w;
  }
  
  void setHeight(int h)
  {
    rectHeight = h;
  }
  void display() {
    fill(122);
    stroke(0);
    line (_var_x + rectWidth*0.5, _var_y, _var_x + rectWidth*0.5, _var_y+ _var_height);
    rect(rectPosX, rectPosY, 
      rectWidth, rectHeight);
 
    // Three small lines in the center   
    centerLine(-3); 
    centerLine(0);
    centerLine(3);
  }
 
  void centerLine(float offset) {
    line(rectPosX+3, rectPosY+rectHeight/2+offset, 
      rectPosX+rectWidth-3, rectPosY+rectHeight/2+offset);
  }
 
  void mousePressedRect() {
    if (mouseOver()) {
      holdScrollRect=true;
      offsetMouseY=mouseY-rectPosY;
    }
  }
 
  void mouseReleasedRect() {
    holdScrollRect=false;
  }
 
  void update() {
    // dragging of the mouse 
    if (holdScrollRect) {
      rectPosY=mouseY-offsetMouseY;
      if (rectPosY<_var_y)
        rectPosY=_var_y;
      if (rectPosY+rectHeight >(_var_y + _var_height)-1)
        rectPosY=(_var_y + _var_height)-rectHeight;
    }
  }
 
  float scrollValue() {
    return map(rectPosY, _var_y , (_var_y + _var_height)-rectHeight, 0, 1000) * 0.001;
  }
 
  boolean mouseOver() {
    return mouseX>rectPosX&&
      mouseX<rectPosX+rectWidth&&
      mouseY>rectPosY&&
      mouseY<rectPosY+rectHeight;
  }//function 
  //
}//class 
//