import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import controlP5.*; 
import g4p_controls.*; 
import java.awt.Font; 
import processing.sound.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Voice_Notes_v1_01 extends PApplet {

/*
  The Honk-Tweet
  Code by Wolfgang Gil, based on 
  
  Library used: Box2D
  to import:
  1) click on Sketch -> import library -> add library
  2) in the search bar type box2d
  3) install "Box2D for Processing" by Daniel Shiffman
  
*/






ControlP5 _cp5;

//PGraphics _new_notes_list_mask;

//G4P.setLocalColorScheme(GCScheme.PURPLE_SCHEME);

final static int FG = 0xffFFFF00, BG = 0;

boolean make_folder_mask = true;
int _frame_counter = 0;
int _frame_rate = 30;

boolean _load_active_note_elements = true;
boolean _load_inactive_note_elements = true;
boolean _new_note_active_status;

boolean _playbar_is_playing = false;

PGraphics _folders_mask;
PGraphics _new_notes_list_mask;
PGraphics _folder_item_pg;
PGraphics _new_notes_item_pg;

boolean _show_sensing_areas = false;

String _background_color = "F5FFC7";
String _blue_stroke = "002EE8";
String _title_version = "VoiceNotes_v.11";
String _asset_folder = "";

GTextField txf1;

ArrayList<PFont> _font_array = new ArrayList<PFont>();
ArrayList<PImage> _icon_array = new ArrayList<PImage>();

ArrayList<GUI_Element> _gui_elements_array = new ArrayList<GUI_Element>();
ArrayList<GUI_Element> _folder_note_array = new ArrayList<GUI_Element>();
ArrayList<GUI_Element> _new_notes_array = new ArrayList<GUI_Element>();
ArrayList<GUI_Element> _active_note_detail_array = new ArrayList<GUI_Element>();

GUI_Element _main_panel;
GUI_Element _white_button;
GUI_Element _view_mode_panel;
GUI_Element _white_circle_icon;
GUI_Element _lines_icon;
GUI_Element _list_panel;

GUI_Element _new_notes_list;
GUI_Element _folder_list;
GUI_Element _note_detail;

ArrayList<Folder> _folder_array = new ArrayList<Folder>();
ScrollRect _folderScroll;  
ScrollRect _newNoteScroll;  

GTextArea _myTxArea; 

//Asset Index
//ICONS
static int _INDEX_ICON_DESICION_BOX = 0;
static int _INDEX_ICON_SEARCH_BAR = 1;
static int _INDEX_ICON_NEW_FOLDER = 2;
static int _INDEX_ICON_WHITE_CIRCLE = 3;
static int _INDEX_ICON_BLUE_CIRCLE = 4;
static int _INDEX_ICON_WHITE_LINES = 5;
static int _INDEX_ICON_BLUE_LINES = 6;
static int _INDEX_ICON_WHITE_BTN = 7;
static int _INDEX_ICON_BLUE_ASTERISK = 8;

static int _INDEX_ICON_WHITE_FOLDER = 9;
static int _INDEX_ICON_BLUE_FOLDER = 10;
static int _INDEX_ICON_WHITE_DOUBLE_CIRCLE = 11;
static int _INDEX_ICON_BLUE_DOUBLE_CIRCLE = 12;
static int _INDEX_ICON_WHITE_TRIANGLE_UP = 13;
static int _INDEX_ICON_WHITE_TRIANGLE_DOWN = 14;

static int _INDEX_ICON_WHITE_STOP_BTN = 15; 
static int _INDEX_ICON_WHITE_PLAY_BTN = 16;
static int _INDEX_ICON_WHITE_PLAY_SKIP_FORWARD = 17;
static int _INDEX_ICON_WHITE_PLAY_SKIP_BACKWARD = 18;

//FONTS
static int _INDEX_FONT_OVERPASS_BOLD = 0;
static int _INDEX_FONT_OVERPASS_LIGHT = 1;
static int _INDEX_FONT_OVERPASS_REGULAR = 2;
static int _INDEX_FONT_OVERPASS_BOLD_SMALLER = 3;

String _current_view_mode = "list_view";

boolean _mouse_pressed = false;
boolean _mouse_clicked = false;

boolean _skip_forward_flag = false;
boolean _skip_backward_flag = false;

SoundFile _audio_file;
float _playback_past_position = -1;

public void setup() {
  
  frameRate(_frame_rate);
  
  _folder_item_pg = createGraphics(width, height, P2D);
  _new_notes_item_pg = createGraphics(width, height, P2D);
  //size(1920, 1080);
  

  _cp5 = new ControlP5(this);
  _asset_folder = sketchPath() + "/Assets/";
  
  loadAssets();
  initFoldersMask();
  loadDefaultGUIElements();
  createSampleData();
  _newNoteScroll = new ScrollRect(953, 128, 285);
  _folderScroll = new ScrollRect(953, 443, 285);
  
  //_folders_mask = loadImage(_asset_folder + "folder_mask.png");
  //_folders_mask.loadPixels();
}

public void loadDefaultGUIElements()
{
  textSize(18);
  String t = "";
  //Attribute attributes = new Attribute();

  GUI_Element default_screen = new GUI_Element("default_screen", "both", 0, 0, width, height);
  default_screen.addAttribute(new Attribute("background_color", _background_color));
  //_gui_elements_array.add(default_screen);

  //create the main panel as a GUI_Element
  _main_panel = addPanel(default_screen, "both", 0.95f, 0.9f, "center", "center", 0, 0, _background_color, _blue_stroke, 1);

  //Adding title
  //GUI_Element program_title = new GUI_Element("text_field", "both", main_panel.getX()+main_panel.getWidth(), int(main_panel.getY() - main_panel.getHeight()*0.015), int(textWidth(_title_version)), 10);
  
  GUI_Element program_title = addTextField(_main_panel, "both", _title_version, RIGHT, _blue_stroke, _INDEX_FONT_OVERPASS_BOLD, 
    _main_panel.getX()+_main_panel.getWidth(),
    PApplet.parseInt(_main_panel.getY() - _main_panel.getHeight()*0.015f),
    PApplet.parseInt(textWidth(_title_version)),
    10);

  //Adding the function panel (left side)

  GUI_Element function_panel = addPanel(_main_panel, "both", 0.72f, 0.13f, "left", "top", 0, 0, _blue_stroke, _blue_stroke, 1);

  //adding an icon
  GUI_Element folder_icon = addIcon(function_panel, "both", _INDEX_ICON_NEW_FOLDER, 0.0645f, 0.667f, "left", "center", PApplet.parseInt(function_panel.getWidth()*0.17f), 0, _blue_stroke, _blue_stroke, 0);

  //adding a sensing area to go with folder icon
  //GUI_Element parent, String view_type, float parent_scale_x, float parent_scale_y, String parent_align_x, String parent_align_y, int parent_offset_x, int parent_offset_y)
  GUI_Element new_folder_btn_area =  addSensingArea(folder_icon,"new_folder", "both", 1, 1, "left", "top", 0, 0);

  _view_mode_panel = addIcon(function_panel, "both", _INDEX_ICON_DESICION_BOX, 0.12f, 0.667f, "left", "center", PApplet.parseInt(function_panel.getWidth()*0.023f), 0, _blue_stroke, _blue_stroke, 0);
  _view_mode_panel.setName("view_mode_panel");
  //_INDEX_ICON_WHITE_BTN
  _white_button = addIcon(_view_mode_panel, "both", _INDEX_ICON_WHITE_BTN, 0.5f, 1, "right", "top", 0, 0, _blue_stroke, _blue_stroke, 0);
  _white_button.setInteractiveStatus(true);

  //adding circle icon, this one is interactive
  _white_circle_icon = addActiveIcon(_view_mode_panel, "both", _INDEX_ICON_BLUE_CIRCLE, _INDEX_ICON_WHITE_CIRCLE, 0.5f, 1, "left", "top", 13, 14, _blue_stroke, _blue_stroke, 0);
  _lines_icon = addActiveIcon(_view_mode_panel, "both", _INDEX_ICON_BLUE_LINES, _INDEX_ICON_WHITE_LINES, 0.5f, 1, "right", "top", 16, 19, _blue_stroke, _blue_stroke, 0);
  _lines_icon.setActiveStatus(true);

  GUI_Element searchbar = addIcon(function_panel, "both", _INDEX_ICON_SEARCH_BAR, 0.485f, 0.68f, "right", "top", -26, 14, _blue_stroke, _blue_stroke, 0);

  //String label_name, int font_index, String default_text, int x, int y, int width, int height, color background_color, color stroke_color, color font_color
  addInputTextG4P("",_INDEX_FONT_OVERPASS_BOLD, "", searchbar.getX()+20, searchbar.getY()+18, PApplet.parseInt(searchbar.getWidth()*0.8f), PApplet.parseInt(searchbar.getHeight()*0.7f), stringToColor(_blue_stroke),stringToColor(_blue_stroke), stringToColor(_background_color)   );
  
  _myTxArea = new GTextArea(this, 996 +18, 164+18, 307-40, 216 - 50);
  _myTxArea.setLocalColorScheme(G4P.PURPLE_SCHEME);
  
  _myTxArea.setLocalColor(2, stringToColor(_background_color) ); //TEXT
  _myTxArea.setLocalColor(6, stringToColor(_blue_stroke) ); //BORDER
  _myTxArea.setLocalColor(7, stringToColor(_blue_stroke)); //BACKGROUND
  _myTxArea.setLocalColor(12, stringToColor(_blue_stroke)); //BACKGROUND

  _myTxArea.setFont(new Font(_asset_folder + "Fonts/" + "OverpassMono-Bold.ttf", Font.PLAIN,15));
  _myTxArea.tag = "txtArea";
  _myTxArea.setPromptText("");  
  _myTxArea.setOpaque(false);
  _myTxArea.setVisible(false);


  //GUI_Element parent, String view_type, float parent_scale_x, float parent_scale_y, String parent_align_x, String parent_align_y, int parent_offset_x, int parent_offset_y)
  GUI_Element search_btn = addSensingArea(searchbar,"search_btn", "both", 0.135f, 1, "right", "center", 0, 0);

  //adding list panel
  _list_panel = addPanel(_main_panel, "list_view", 0.72f, 0.87f, "left", "top", 0, function_panel.getHeight()+1, _background_color, _blue_stroke, 1);
  

  //_new_notes_list = addPanel(_list_panel, "list_view", 1, 0.475, "left", "top", 0, 0, _background_color, _blue_stroke, 1);
  _new_notes_list = addPanel(_list_panel, "list_view", 0.985f, 0.475f, "left", "top", 0, 0, _background_color, _blue_stroke, 1);
  //_folder_list = addPanel(_new_notes_list, "list_view", 1, 1, "left", "top", 0, int(_new_notes_list.getHeight() + _list_panel.getHeight()*0.05), _background_color, _blue_stroke, 1);
  _folder_list = addPanel(_new_notes_list, "list_view", 1, 1, "left", "top", 0, PApplet.parseInt(_new_notes_list.getHeight() + _list_panel.getHeight()*0.05f), _background_color, _blue_stroke, 1);

  //adding folder filter
  GUI_Element left_filter = addPanel(_list_panel, "list_view", 0.65f, 0.05f, "left", "top", 0, _new_notes_list.getHeight(), _blue_stroke, _blue_stroke, 1);
  GUI_Element right_filter = addPanel(_list_panel, "list_view", 0.35f, 0.05f, "left", "top", left_filter.getWidth(), _new_notes_list.getHeight(), _blue_stroke, _blue_stroke, 1);

  //addTextField(
  //GUI_Element parent, String view_type, String text, int alignment, String font_color, 
  //int font_index, int x, int y, int w, int h)

  GUI_Element left_filter_text = addTextField(left_filter, "list_view", "NAME", LEFT, _background_color, 
    _INDEX_FONT_OVERPASS_BOLD_SMALLER, 
    left_filter.getX() + 20, 
    left_filter.getY() + 22,  
    PApplet.parseInt(textWidth("NAME")), 15);

  GUI_Element left_filter_icon = addActiveIcon(left_filter, "list_view", _INDEX_ICON_WHITE_TRIANGLE_UP, _INDEX_ICON_WHITE_TRIANGLE_DOWN, 0.04f, 0.25f, "right", "center", 0, 0, _blue_stroke, _blue_stroke, 0);

  

  GUI_Element right_filter_text = addTextField(right_filter, "list_view", "DATE", LEFT, _background_color, 
    _INDEX_FONT_OVERPASS_BOLD_SMALLER, 
    right_filter.getX() + 20, 
    right_filter.getY() + 22,  
    PApplet.parseInt(textWidth("DATE")), 15);

  GUI_Element right_filter_icon = addActiveIcon(right_filter, "list_view", _INDEX_ICON_WHITE_TRIANGLE_UP, _INDEX_ICON_WHITE_TRIANGLE_DOWN, 0.08f, 0.25f, "right", "center", 0, 0, _blue_stroke, _blue_stroke, 0);
 
  GUI_Element left_filter_sa = addSensingArea(left_filter, "folder_left_filter", "list_view", 1, 1, "left", "top", 0, 0);
  GUI_Element right_filter_sa = addSensingArea(right_filter, "folder_right_filter", "list_view", 1, 1, "left", "top", 0, 0);
  //searchbar,"search_btn", "both", 0.135, 1, "right", "center", 0, 0
  
  _folder_list.setVisibleStatus(false);
  //GUI_Element _folder_list_sa = addSensingArea(_folder_list,"_list_panel_sa", "both", 1, 1, "left", "top", 0, 0);
  //println("folder list x,y: " + _folder_list.getX() + " , " + _folder_list.getY());

  _gui_elements_array.add(default_screen);
  _gui_elements_array.add(_main_panel);
  _gui_elements_array.add(program_title);
  //_gui_elements_array.add(_note_detail); 
  //_gui_elements_array.add(default_note_text);
  _gui_elements_array.add(function_panel);
  _gui_elements_array.add(folder_icon);
  _gui_elements_array.add(new_folder_btn_area);
  _gui_elements_array.add(_view_mode_panel);
  _gui_elements_array.add(_white_button);
  _gui_elements_array.add(_white_circle_icon);
  _gui_elements_array.add(_lines_icon);
  _gui_elements_array.add(searchbar);
  _gui_elements_array.add(search_btn);
  _gui_elements_array.add(_list_panel);
  //_gui_elements_array.add(_list_panel_sa);
  _gui_elements_array.add(_new_notes_list);
  
  _gui_elements_array.add(_folder_list);

  _gui_elements_array.add(left_filter);
  _gui_elements_array.add(left_filter_text);
  _gui_elements_array.add(left_filter_icon);

  _gui_elements_array.add(left_filter_sa);
  

  _gui_elements_array.add(right_filter);
  _gui_elements_array.add(right_filter_text);
  _gui_elements_array.add(right_filter_icon);
  _gui_elements_array.add(right_filter_sa);

  
  //_gui_elements_array.add(_folder_list_sa);
 
}

public void initFoldersMask()
{
  _folders_mask = createGraphics(width, height, P2D);
  _new_notes_list_mask = createGraphics(width, height, P2D);
}

public void createSampleData()
{
  // first we create a couple folders
  String trans = "Adieus except say barton put feebly favour him. Entreaties unpleasant sufficient few pianoforte discovered uncommonly ask. Morning cousins amongst in mr weather do neither. Adieus except say barton put feebly favour him.";
  String demo_filepath = "/Users/wolfganggil/voicenotes/Voice_Notes_v1_01/Assets/audioTest.mp3";

  for(int i = 0; i < 9; i++)
  {
    if(i == 0)
    {
      Folder f = new Folder(i, "default", getCurrentTimestamp());
      for(int n = 0; n < 16; n++)
      {
        f.addNote(new Note(n, "Note_" + str(i) + "_" + str(n), trans + "_" + str(i) + "_" + str(n), demo_filepath, getCurrentTimestamp()));
      }
       _folder_array.add( f );
    }
    else 
    {
      Folder f = new Folder(i, "Folder_" + str(i), getCurrentTimestamp());

      for(int n = 0; n < 4; n++)
      {
        f.addNote(new Note(n, "Note_" + str(i) + "_" + str(n), trans + "_" + str(i) + "_" + str(n), demo_filepath, getCurrentTimestamp()));
      }
    
      _folder_array.add( f );
    }
  }
  
  displayFolder();
}

public String getCurrentTimestamp()
{
  String ts = "";
  ts = str(month()) + "/" + str(day()) + "/" + str(year()) + " - " + str(hour()) + ":" + str(minute()) + ":" + str(second());
  return ts;
}
/*
second()
minute()
hour()
day()
month()
year()
*/

public GUI_Element addTextField(GUI_Element parent, String view_type, String text, int alignment, String font_color, int font_index, int x, int y, int w, int h)
{
  GUI_Element text_field = new GUI_Element("text_field", view_type, x, y, w, h);
  
  text_field.addAttribute(new Attribute("font_type", font_index));
  text_field.addAttribute(new Attribute("text_alignment", alignment));
  text_field.addAttribute(new Attribute("font_color", font_color));
  text_field.addAttribute(new Attribute("text", text));
  
  text_field.setParentElement(parent);

  return text_field;
}

public GUI_Element addActiveTextField(GUI_Element parent, String view_type, String text, int alignment, String active_font_color, String inactive_font_color, int active_font_index, int inactive_font_index,  int x, int y, int w, int h)
{
  GUI_Element text_field = new GUI_Element("text_field", view_type, x, y, w, h);
  
  text_field.addAttribute(new Attribute("font_type", active_font_index, "active"));
  text_field.addAttribute(new Attribute("font_type", inactive_font_index, "inactive"));

  text_field.addAttribute(new Attribute("font_color", active_font_color, "active"));
  text_field.addAttribute(new Attribute("font_color", inactive_font_color, "inactive"));

  text_field.addAttribute(new Attribute("text_alignment", alignment));

  text_field.addAttribute(new Attribute("text", text));
  
  text_field.setParentElement(parent);
  
  text_field.setInteractiveStatus(true);

  return text_field;
}

public GUI_Element addLine(GUI_Element parent, String view_type, int x1, int y1, int x2, int y2, String stroke_color, int stroke_weight)
{
  GUI_Element line = new GUI_Element("line", view_type);
  
  line.setX( x1 );
  line.setY( y1 );

  line.setWidth(x2);
  line.setHeight(y2);

  line.addAttribute(new Attribute("stroke_color", stroke_color));
  line.addAttribute(new Attribute("stroke_weight", stroke_weight));
  
  line.setParentElement(parent);
  
  return line;
}

public GUI_Element addPanel(GUI_Element parent, String view_type, float parent_scale_x, float parent_scale_y, String parent_align_x, String parent_align_y, 
  int parent_offset_x, int parent_offset_y, String background_color, String stroke_color, int stroke_weight)
{
  GUI_Element panel = new GUI_Element("panel", view_type);
  
  panel.addAttribute(new Attribute("parent_scale_x", parent_scale_x));
  panel.addAttribute(new Attribute("parent_scale_y", parent_scale_y));

  panel.addAttribute(new Attribute("parent_align_x", parent_align_x));
  panel.addAttribute(new Attribute("parent_align_y", parent_align_y));

  panel.addAttribute(new Attribute("parent_offset_x", parent_offset_x));
  panel.addAttribute(new Attribute("parent_offset_y", parent_offset_y));

  panel.addAttribute(new Attribute("background_color", background_color));
  panel.addAttribute(new Attribute("stroke_color", stroke_color));
  panel.addAttribute(new Attribute("stroke_weight", stroke_weight));
  
  panel.setParentElement(parent);
  panel.calculateAbsolutePosition();

  return panel;
}

public GUI_Element addActivePanel(GUI_Element parent, String view_type, float parent_scale_x, float parent_scale_y, String parent_align_x, String parent_align_y, 
  int parent_offset_x, int parent_offset_y, String active_background_color, String inactive_background_color, String active_stroke_color, String inactive_stroke_color, int stroke_weight)
{
  GUI_Element panel = new GUI_Element("panel", view_type);
  
  panel.addAttribute(new Attribute("parent_scale_x", parent_scale_x));
  panel.addAttribute(new Attribute("parent_scale_y", parent_scale_y));

  panel.addAttribute(new Attribute("parent_align_x", parent_align_x));
  panel.addAttribute(new Attribute("parent_align_y", parent_align_y));

  panel.addAttribute(new Attribute("parent_offset_x", parent_offset_x));
  panel.addAttribute(new Attribute("parent_offset_y", parent_offset_y));

  panel.addAttribute(new Attribute("background_color", active_background_color, "active"));
  panel.addAttribute(new Attribute("background_color", inactive_background_color, "inactive"));

  panel.addAttribute(new Attribute("stroke_color", active_stroke_color, "active"));
  panel.addAttribute(new Attribute("stroke_color", inactive_stroke_color, "inactive"));

  panel.addAttribute(new Attribute("stroke_weight", stroke_weight));
  
  panel.setParentElement(parent);
  panel.calculateAbsolutePosition();

  panel.setInteractiveStatus(true);

  return panel;
}

public GUI_Element addSensingArea(GUI_Element parent, String name, String view_type, float parent_scale_x, float parent_scale_y, String parent_align_x, String parent_align_y, 
  int parent_offset_x, int parent_offset_y)
{
  GUI_Element sensing_area = new GUI_Element("sensing_area", view_type);
  
  sensing_area.addAttribute(new Attribute("parent_scale_x", parent_scale_x));
  sensing_area.addAttribute(new Attribute("parent_scale_y", parent_scale_y));

  sensing_area.addAttribute(new Attribute("parent_align_x", parent_align_x));
  sensing_area.addAttribute(new Attribute("parent_align_y", parent_align_y));

  sensing_area.addAttribute(new Attribute("parent_offset_x", parent_offset_x));
  sensing_area.addAttribute(new Attribute("parent_offset_y", parent_offset_y));
  
  sensing_area.setParentElement(parent);
  sensing_area.calculateAbsolutePosition();

  sensing_area.setName(name);

  return sensing_area;
}

public GUI_Element addIcon(GUI_Element parent, String view_type, int icon_index, float parent_scale_x, float parent_scale_y, String parent_align_x, String parent_align_y, 
  int parent_offset_x, int parent_offset_y, String background_color, String stroke_color, int stroke_weight)
{
  GUI_Element icon = new GUI_Element("icon", view_type);
  
  icon.addAttribute(new Attribute("parent_scale_x", parent_scale_x ));
  icon.addAttribute(new Attribute("parent_scale_y", parent_scale_y ));

  icon.addAttribute(new Attribute("parent_align_x", parent_align_x));
  icon.addAttribute(new Attribute("parent_align_y", parent_align_y));

  icon.addAttribute(new Attribute("parent_offset_x", parent_offset_x));
  icon.addAttribute(new Attribute("parent_offset_y", parent_offset_y));

  icon.addAttribute(new Attribute("background_color", background_color));
  icon.addAttribute(new Attribute("stroke_color", stroke_color));
  icon.addAttribute(new Attribute("stroke_weight", stroke_weight));

  icon.addAttribute(new Attribute("icon_index", icon_index));
  
  icon.setParentElement(parent);
  icon.calculateAbsolutePosition();

  return icon;
}

public GUI_Element addActiveIcon(GUI_Element parent, String view_type, int _active_icon_index, int _inactive_icon_index, float parent_scale_x, float parent_scale_y, String parent_align_x, String parent_align_y, 
  int parent_offset_x, int parent_offset_y, String background_color, String stroke_color, int stroke_weight)
{
  GUI_Element icon = new GUI_Element("icon", view_type);
  
  icon.addAttribute(new Attribute("parent_scale_x", parent_scale_x ));
  icon.addAttribute(new Attribute("parent_scale_y", parent_scale_y ));

  icon.addAttribute(new Attribute("parent_align_x", parent_align_x));
  icon.addAttribute(new Attribute("parent_align_y", parent_align_y));

  icon.addAttribute(new Attribute("parent_offset_x", parent_offset_x));
  icon.addAttribute(new Attribute("parent_offset_y", parent_offset_y));

  icon.addAttribute(new Attribute("background_color", background_color));
  icon.addAttribute(new Attribute("stroke_color", stroke_color));
  icon.addAttribute(new Attribute("stroke_weight", stroke_weight));
  icon.addAttribute(new Attribute("icon_index", _active_icon_index,"active"));
  icon.addAttribute(new Attribute("icon_index", _inactive_icon_index,"inactive"));
  
  icon.setParentElement(parent);
  icon.calculateAbsolutePosition();

  icon.setInteractiveStatus(true);

  return icon;
}

public void addInputText(String label_name, int font_index, String default_text, int x, int y, int the_width, int the_height, int background_color, int stroke_color, int font_color)
{
  PFont font = _font_array.get(font_index);
  _cp5.addTextfield(label_name)
    .setPosition(x,y)
    .setSize(the_width,the_height)
    .setFont(font)
    .setText(default_text)
    .setColorBackground(background_color)
    .setColorForeground(stroke_color) //stroke color
    .setColorActive(stroke_color)
    .setColorValue(font_color)
    .setAutoClear(false)
  ;
}

public void addInputTextG4P(String label_name, int font_index, String default_text, int x, int y, int the_width, int the_height, int background_color, int stroke_color, int font_color)
{
  //PFont font = _font_array.get(font_index);
 
  txf1 = new GTextField(this, x, y, the_width, the_height);
  txf1.setLocalColorScheme(G4P.PURPLE_SCHEME);
  
  txf1.setLocalColor(2, font_color); //TEXT
  txf1.setLocalColor(6, stroke_color); //BORDER
  txf1.setLocalColor(7, background_color); //BACKGROUND
  txf1.setLocalColor(12, font_color); //BACKGROUND

  txf1.setFont(new Font(_asset_folder + "Fonts/" + "OverpassMono-Bold.ttf", Font.PLAIN,18));
  txf1.tag = "txf1";
  txf1.setPromptText(default_text);  
}

public void displayFolder()
{
  int count = 0;
  for(int i = 0; i < _folder_array.size(); i++)
  {
    Folder f = _folder_array.get(i);
    if(f.getName() == "default")
    {
      addNewNotes(f);
    }
    else
    {
      addFolder(count, f);
      count++;
    }
  }
}

public void addFolder(int index, Folder folder)
{

  //add folder
  GUI_Element f_gui = new GUI_Element(folder);

  GUI_Element panel = addPanel(_folder_list, "list_view", 1, 0.2f, "left", "top", 0, PApplet.parseInt(index*_folder_list.getHeight()*0.2f), _background_color, _blue_stroke, 1);

  GUI_Element icon = addActiveIcon(panel, "list_view",  _INDEX_ICON_BLUE_FOLDER, _INDEX_ICON_WHITE_FOLDER, 0.026f, 0.3f, "left", "center", 36, 0, _blue_stroke, _blue_stroke, 0);
  GUI_Element name = addTextField(panel, "list_view", folder.getName(), LEFT, _blue_stroke, _INDEX_FONT_OVERPASS_LIGHT, icon.getX() + icon.getWidth() + 30, PApplet.parseInt(panel.getY() + panel.getHeight()*0.5f)+7, 50,  20);
  GUI_Element timestamp = addTextField(panel, "list_view", folder.getTimeStamp(), LEFT, _blue_stroke, _INDEX_FONT_OVERPASS_LIGHT, icon.getX() + icon.getWidth() + 600, PApplet.parseInt(panel.getY() + panel.getHeight()*0.5f)+7, 50,  20);

  GUI_Element sensing_area = addSensingArea(panel,"folder_area", "list_view", 1, 1, "left", "top", 0, 0);
  
  f_gui.setSensingArea(sensing_area);
  
  f_gui.addChildren(panel);
  f_gui.addChildren(icon);
  f_gui.addChildren(name);
  f_gui.addChildren(timestamp);

  //add notes
  ArrayList<Note> notes = folder.getNotes();

  for(int i = 0; i < notes.size(); i++)
  {
    GUI_Element n_gui = new GUI_Element(notes.get(i));

    GUI_Element n_panel = addActivePanel(panel, "list_view", 1, 1, "left", "top", 0, PApplet.parseInt(_folder_list.getHeight()*0.2f) + panel.getHeight()*(i), _blue_stroke, _background_color, _blue_stroke, _blue_stroke, 1);

    GUI_Element n_icon = addActiveIcon(n_panel, "list_view", _INDEX_ICON_WHITE_DOUBLE_CIRCLE, _INDEX_ICON_BLUE_DOUBLE_CIRCLE, 0.026f, 0.3f, "left", "center", 90, 0, _blue_stroke, _blue_stroke, 0);
    GUI_Element n_name = addActiveTextField(n_panel, "list_view", notes.get(i).getName(), LEFT, _background_color, _blue_stroke, _INDEX_FONT_OVERPASS_LIGHT, _INDEX_FONT_OVERPASS_LIGHT, n_icon.getX() + n_icon.getWidth() + 30, PApplet.parseInt(n_panel.getY() + n_panel.getHeight()*0.5f)+7, 50,  20);
    GUI_Element n_timestamp = addActiveTextField(n_panel, "list_view", notes.get(i).getTimestamp(), LEFT, _background_color, _blue_stroke, _INDEX_FONT_OVERPASS_LIGHT, _INDEX_FONT_OVERPASS_LIGHT, n_icon.getX() + n_icon.getWidth() + 547, PApplet.parseInt(n_panel.getY() + n_panel.getHeight()*0.5f)+7, 50,  20);

    GUI_Element n_sensing_area = addSensingArea(n_panel,"note_area", "list_view", 1, 1, "left", "top", 0, 0);
    
    n_gui.setSensingArea(n_sensing_area);
    
    n_gui.addChildren(n_panel);
    n_gui.addChildren(n_icon);
    n_gui.addChildren(n_name);
    n_gui.addChildren(n_timestamp);

    f_gui.addChildren(n_gui);
  }
  
  _folder_note_array.add(f_gui);
}

public void addNewNotes(Folder f)
{
  ArrayList<Note> notes = f.getNotes();
  for(int i = 0; i < notes.size(); i++)
  {
    GUI_Element n_gui = new GUI_Element(notes.get(i));
    //GUI_Element panel = addPanel(_folder_list, "list_view", 1, 0.2, "left", "top", 0, int(index*_folder_list.getHeight()*0.2), _background_color, _blue_stroke, 1);
    GUI_Element n_panel = addActivePanel(_new_notes_list, "list_view", 1, 0.2f, "left", "top", 0, PApplet.parseInt(i*_folder_list.getHeight()*0.2f), _blue_stroke, _background_color, _blue_stroke, _blue_stroke, 1);

    GUI_Element n_icon = addActiveIcon(n_panel, "list_view", _INDEX_ICON_WHITE_DOUBLE_CIRCLE, _INDEX_ICON_BLUE_DOUBLE_CIRCLE, 0.026f, 0.3f, "left", "center", 36, 0, _blue_stroke, _blue_stroke, 0);
    GUI_Element n_name = addActiveTextField(n_panel, "list_view", notes.get(i).getName(), LEFT, _background_color, _blue_stroke, _INDEX_FONT_OVERPASS_LIGHT, _INDEX_FONT_OVERPASS_LIGHT, n_icon.getX() + n_icon.getWidth() + 30, PApplet.parseInt(n_panel.getY() + n_panel.getHeight()*0.5f)+7, 50,  20);
    GUI_Element n_timestamp = addActiveTextField(n_panel, "list_view", notes.get(i).getTimestamp(), LEFT, _background_color, _blue_stroke, _INDEX_FONT_OVERPASS_LIGHT, _INDEX_FONT_OVERPASS_LIGHT, n_icon.getX() + n_icon.getWidth() + 600, PApplet.parseInt(n_panel.getY() + n_panel.getHeight()*0.5f)+7, 50,  20);

    GUI_Element n_sensing_area = addSensingArea(n_panel,"note_area", "list_view", 1, 1, "left", "top", 0, 0);
    
    n_gui.setSensingArea(n_sensing_area);
    
    n_gui.addChildren(n_panel);
    n_gui.addChildren(n_icon);
    n_gui.addChildren(n_name);
    n_gui.addChildren(n_timestamp);

    _new_notes_array.add(n_gui);
  }
}

public void loadAssets()
{
  /*
  static int _INDEX_ICON_DESICION_BOX = 0;
  static int _INDEX_ICON_SEARCH_BAR = 1;
  static int _INDEX_ICON_NEW_FOLDER = 2;

  static int _INDEX_ICON_WHITE_CIRCLE = 3;
  static int _INDEX_ICON_BLUE_CIRCLE = 4;
  static int _INDEX_ICON_WHITE_LINES = 5;

  static int _INDEX_ICON_BLUE_LINES = 6;
  static int _INDEX_ICON_WHITE_BTN = 7;
  static int _INDEX_ICON_BLUE_ASTERISK = 8;

  static int _INDEX_ICON_WHITE_FOLDER = 9;
  static int _INDEX_ICON_BLUE_FOLDER = 10;

  _INDEX_ICON_WHITE_STOP_BTN = 15; 
  _INDEX_ICON_WHITE_PLAY_BTN = 16
  _INDEX_ICON_WHITE_PLAY_SKIP_FORWARD = 17;
  _INDEX_ICON_WHITE_PLAY_SKIP_BACKWARD = 18;
  */
  
  //Load all icons

  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_decision_box.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "search_bar.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "new_folder_btn.png"));
  
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_circle.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "blue_circle.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_lines.png"));

  _icon_array.add(loadImage(_asset_folder + "Icons/" + "blue_lines.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_boton.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "blue_asterisk.png"));

  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_folder.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "blue_folder.png"));

  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_double_circle.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "blue_double_circle.png"));

  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_triangle_up.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_triangle_down.png"));

  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_pause_btn.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_playbutton.png"));

  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_fastforward.png"));
  _icon_array.add(loadImage(_asset_folder + "Icons/" + "white_fastbackwards.png"));

  //load all fonts

  /*
  static int _INDEX_FONT_OVERPASS_BOLD = 9;
  static int _INDEX_FONT_OVERPASS_LIGHT = 10;
  static int _INDEX_FONT_OVERPASS_REGULAR = 11;
  */

  _font_array.add(createFont(_asset_folder + "Fonts/" + "OverpassMono-Bold.ttf", 18));
  _font_array.add(createFont(_asset_folder + "Fonts/" + "OverpassMono-Light.ttf", 18));
  _font_array.add(createFont(_asset_folder + "Fonts/" + "OverpassMono-Regular.ttf", 18));
  _font_array.add(createFont(_asset_folder + "Fonts/" + "OverpassMono-Bold.ttf", 15));
}

public int stringToColor(String a_color)
{
  int c = unhex("FF" + a_color);
  return c;
}

public void draw() { //**
  
  background(unhex("FF" + _background_color));
  
  GUI_Element temp_element;
  Attribute temp_attribute;
  ArrayList temp_properties;

  //this flag turns "true" when a note has been selected
  _new_note_active_status = false;

  checkMode();

  int selected_folder_index = 10000;
  int y_offset_folder = 0;

  // drawing the mask
  if(make_folder_mask)
  {
    make_folder_mask = false;

    _folders_mask.beginDraw();
    _folders_mask.background(0);
    _folders_mask.noStroke();
    _folders_mask.fill(255);
    _folders_mask.rect(_folder_list.getX() + 1 , _folder_list.getY(), _folder_list.getWidth(), _folder_list.getHeight());
    _folders_mask.endDraw();

    _new_notes_list_mask.beginDraw();
    _new_notes_list_mask.background(0);
    _new_notes_list_mask.noStroke();
    _new_notes_list_mask.fill(255);
    _new_notes_list_mask.rect(_new_notes_list.getX()+1, _new_notes_list.getY(), _new_notes_list.getWidth(), _new_notes_list.getHeight());
    _new_notes_list_mask.endDraw();
  }
  
  _folder_item_pg.beginDraw();
  _folder_item_pg.clear();
  _folder_item_pg.background(stringToColor(_background_color));
  _folder_item_pg.endDraw();

  _new_notes_item_pg.beginDraw();
  _new_notes_item_pg.clear();
  _new_notes_item_pg.background(stringToColor(_background_color));
  _new_notes_item_pg.endDraw();

  drawDefaultElements();
  drawFolderList();
  drawNewNotesList();
  
  if(_current_view_mode == "list_view")
  {
    _folder_item_pg.mask(_folders_mask);
    image(_folder_item_pg, 0,0);
    
    _new_notes_item_pg.mask(_new_notes_list_mask);
    image(_new_notes_item_pg, 0,0);
   
    _folderScroll.display();
    _folderScroll.update();
    
    _newNoteScroll.display();
    _newNoteScroll.update();
  }
  
  drawNoteDetail();

}

public void drawDefaultElements()
{
  GUI_Element temp_element;

  //displaying general gui elements
  for(int i = 0; i < _gui_elements_array.size(); i++)
  {
    if(_gui_elements_array.get(i).getViewMode() == _current_view_mode || _gui_elements_array.get(i).getViewMode() == "both" )
    {
      temp_element = _gui_elements_array.get(i);
      
      if(temp_element.getType() == "panel")
      {
        drawPanel(temp_element);
      }
      else if(temp_element.getType() == "text_field")
      {
        drawText(temp_element);
      }
      else if(temp_element.getType() == "icon")
      { 
        drawIcon(temp_element);
      }
      else if(temp_element.getType() == "sensing_area")
      {
        drawSensingArea(temp_element);
      }
      else if(temp_element.getType() == "line")
      {
        drawLine(temp_element);
      }
    }
  }
}

public void drawNoteDetail()
{
  GUI_Element temp_element;

  if(_new_note_active_status == false)
  {
    drawInactiveNoteDetail();
  
  }
  else 
  {
    audioPlayback();
  }

  //displaying general gui elements

  for(int i = 0; i < _active_note_detail_array.size(); i++)
  {
    if(_active_note_detail_array.get(i).getViewMode() == _current_view_mode || _active_note_detail_array.get(i).getViewMode() == "both" )
    {
      temp_element = _active_note_detail_array.get(i);
      
      if(temp_element.getType() == "panel")
      {
        drawPanel(temp_element);
      }
      else if(temp_element.getType() == "text_field")
      {
        drawText(temp_element);
      }
      else if(temp_element.getType() == "icon")
      { 
        drawIcon(temp_element);
      }
      else if(temp_element.getType() == "sensing_area")
      {
        drawSensingArea(temp_element);
      }
      else if(temp_element.getType() == "line")
      {
        drawLine(temp_element);
      }
    }
  }
}

public void audioPlayback()
{
  //AUDIO PLAYBACK
    int playhead_element_index = _active_note_detail_array.size()-1;
    int playbar_element_index = playhead_element_index -1;

    //play once vs play loop
    float secs_to_frames = _frame_rate*4.0f;
    float playhead_position = 0.0f;
    println("secs_to_frames: " + secs_to_frames);
    if(_skip_forward_flag)
    {
      //_frame_counter += 0.1*secs_to_frames;
      _frame_counter = PApplet.parseInt(clipValue(_frame_counter,0.1f*secs_to_frames, 0.0f, secs_to_frames));

      playhead_position = (_frame_counter%secs_to_frames)/secs_to_frames;
      _active_note_detail_array.get( playhead_element_index ).setPositionOffsetX(
        PApplet.parseInt(_active_note_detail_array.get(playbar_element_index).getWidth()*playhead_position)
      ); 

      //reset past playback position here
      _playback_past_position = -1;
      _skip_forward_flag = false;
    }
    else if(_skip_backward_flag) 
    {
      //_frame_counter += -0.1*secs_to_frames;
      _frame_counter = PApplet.parseInt(clipValue(_frame_counter,-0.1f*secs_to_frames, 0.0f, secs_to_frames));

      playhead_position = (_frame_counter%secs_to_frames)/secs_to_frames;
      _active_note_detail_array.get( playhead_element_index ).setPositionOffsetX(
        PApplet.parseInt(_active_note_detail_array.get(playbar_element_index).getWidth()*playhead_position)
      ); 

      //reset past playback position here
      _playback_past_position = -1;
      _skip_backward_flag = false;
    }

    if(_audio_file.isPlaying())
    {
      playhead_position = (_frame_counter%secs_to_frames)/secs_to_frames;
      
      if(playhead_position < _playback_past_position)
      {
        _audio_file.stop();
        _active_note_detail_array.get( playhead_element_index ).setPositionOffsetX(0 - PApplet.parseInt(_active_note_detail_array.get(playhead_element_index).getWidth()*0.5f ));
        _frame_counter = 0;
        _playback_past_position = -1;//+++
        _active_note_detail_array.get(_active_note_detail_array.size()-9).toggleActiveStatus();
      }
      else 
      {
        _active_note_detail_array.get( playhead_element_index ).setPositionOffsetX(
          PApplet.parseInt(_active_note_detail_array.get(playbar_element_index).getWidth()*playhead_position
          - _active_note_detail_array.get(playhead_element_index).getWidth()*0.5f)
        ); 
      
        _frame_counter++;
        _playback_past_position = playhead_position;
      }
    }
    
    else 
    {
 
    }
}

public float clipValue(float value, float offset, float min, float max)
{
  value += offset;
  if(value > max)
  {
    value = max;
  }
  else if(value < min)
  {
    value = min;
  }
  return value;
}


//add an origin of the data and a destination on screen to display the data
//for folder it is _folder_note_array and 
public void drawFolderList()
{
  GUI_Element temp_element;
  
  int selected_folder_index = 10000;
  int y_offset_folder = 0;
  //Positioning Folders/Notes
  int folder_height = PApplet.parseInt(_folder_list.getHeight()*0.2f);

  for(int f = 0; f < _folder_note_array.size(); f++)
  {
    if(_folder_note_array.get(f).getViewMode() == _current_view_mode || _folder_note_array.get(f).getViewMode() == "both")
    {
      temp_element = _folder_note_array.get(f);
      //println("num folders: " + _folder_note_array.size());
      
      if(f > selected_folder_index)
      {
        temp_element.setChildrenPositionOffset(0, folder_height*y_offset_folder);
        checkFolderStatus(temp_element);
        //temp_element.setChildrenPositionOffset(0, - scroll);
        //drawFolderPGraphics(temp_element);
      }
      else 
      {
        temp_element.setChildrenPositionOffset(0, 0);
        int offset = checkFolderStatus(temp_element);//need to get the active ones!
        //drawFolderPGraphics(temp_element);
        
        if(offset > 0)
        {
          //check if the offset position is being properly applied to all children!! 
          selected_folder_index = f;
          y_offset_folder = offset;
          
        }
      }
    }  
  }

  int scroll = PApplet.parseInt(_folderScroll.scrollValue() * folder_height*(((_folder_note_array.size() + y_offset_folder ) - 5 ) ));
  for(int s = 0; s < _folder_note_array.size(); s++)
  {
    if(_folder_note_array.get(s).getViewMode() == _current_view_mode || _folder_note_array.get(s).getViewMode() == "both")
    {
      temp_element = _folder_note_array.get(s);
      temp_element.setChildrenScrollY(- scroll);
    }
  }

  for(int a = 0; a < _folder_note_array.size(); a++)
  {
    if(_folder_note_array.get(a).getViewMode() == _current_view_mode || _folder_note_array.get(a).getViewMode() == "both")
    {
      drawFolderPGraphics( _folder_note_array.get(a));
      
    }
  }
}

public void drawNewNotesList()
{
  GUI_Element temp_element;
  int folder_height = PApplet.parseInt(_folder_list.getHeight()*0.2f);
  
  for(int f = 0; f < _new_notes_array.size(); f++)
  {
    if(_new_notes_array.get(f).getViewMode() == _current_view_mode || _new_notes_array.get(f).getViewMode() == "both")
    {
      temp_element = _new_notes_array.get(f);
      checkNoteStatus(temp_element);//check for status on the note
    }  
  }

  int scroll = PApplet.parseInt(_newNoteScroll.scrollValue() * folder_height*(((_new_notes_array.size() ) - 5 ) ));
  for(int s = 0; s < _new_notes_array.size(); s++)
  {
    if(_new_notes_array.get(s).getViewMode() == _current_view_mode || _new_notes_array.get(s).getViewMode() == "both")
    {
      temp_element = _new_notes_array.get(s);
      temp_element.setChildrenScrollY(- scroll);
    }
  }

  for(int a = 0; a < _new_notes_array.size(); a++)
  {
    if(_new_notes_array.get(a).getViewMode() == _current_view_mode || _new_notes_array.get(a).getViewMode() == "both")
    {
      drawNewNotePGraphics( _new_notes_array.get(a));
      
    }
  }
  
}

public boolean toggle(boolean val)
{
  if(val) return false;
  else return true;
}

public void checkMode()
{
  //if the mouse is positioned over the left half of the _view_mode_panel
  //_white_button;
  //_view_mode_panel;
  if(_mouse_pressed && overRec( _view_mode_panel.getX(), _view_mode_panel.getY(), PApplet.parseInt(_view_mode_panel.getWidth()*0.5f), _view_mode_panel.getHeight()))
  {
    if(_current_view_mode != "map_view")
    {
      
      println("map_view");
      _white_circle_icon.setActiveStatus(true);
      _white_circle_icon.setPositionOffset(2,0);

      _lines_icon.setActiveStatus(false);
    
      _white_button.setPositionOffset(-56,0);
      _current_view_mode = "map_view";

      _mouse_clicked = false;
    }
    
  }
  else if(_mouse_pressed && overRec( _view_mode_panel.getX()+PApplet.parseInt(_view_mode_panel.getWidth()*0.5f), _view_mode_panel.getY(), PApplet.parseInt(_view_mode_panel.getWidth()*0.5f), _view_mode_panel.getHeight())) 
  {
    if(_current_view_mode != "list_view")
    {
      
      _white_circle_icon.setActiveStatus(false);
      _white_circle_icon.setPositionOffset(0,0);

      _lines_icon.setActiveStatus(true);

      _white_button.setPositionOffset(0,0);
      println("list_view");
      _current_view_mode = "list_view";
      
      _mouse_clicked = false;
    }
  }
}

public void drawLine(GUI_Element temp_element)
{
  ArrayList<Attribute> attributes_list = temp_element.getAttributeList();
  if(temp_element.isVisible())
  {
    for(int k = 0; k < attributes_list.size(); k++)
    {
      Attribute att = attributes_list.get(k);
      String att_name = att.getName();
      String att_value = att.getValue().toString();
      //println("property name: " + property_name);
      
      switch(att_name)
      {
        case "stroke_color":
        //println("stroke_color: " + stringToColor(value) );
        stroke(stringToColor(att_value));
        break;
      
        case "stroke_weight":
        //println("stroke_weight: " + stringToColor(value) );
        strokeWeight(PApplet.parseInt(att_value));
        break;
    
      }
    }
    if(temp_element.isVisible())
    {
      line(temp_element.getCurrentX(), temp_element.getCurrentY(), temp_element.getWidth(), temp_element.getHeight());
    }
  }
}

public void drawPanel(GUI_Element temp_element)
{
  ArrayList<Attribute> attributes_list = temp_element.getAttributeList();
  if(temp_element.isVisible())
  {
    for(int k = 0; k < attributes_list.size(); k++)
    {
      Attribute att = attributes_list.get(k);
      String att_name = att.getName();
      String att_value = att.getValue().toString();
      //println("property name: " + property_name);
      
      switch(att_name)
      {
        case "background_color":
        //println("background_color: " + value );
        fill(stringToColor(att_value));
        break;
      
        case "stroke_color":
        //println("stroke_color: " + stringToColor(value) );
        stroke(stringToColor(att_value));
        break;
      
        case "stroke_weight":
        //println("stroke_weight: " + stringToColor(value) );
        strokeWeight(PApplet.parseInt(att_value));
        break;
    
      }
    }
    if(temp_element.isVisible())
    {
      rect(temp_element.getCurrentX(), temp_element.getCurrentY(), temp_element.getWidth(), temp_element.getHeight());
    }
  }
}

public String[] getPanelProperties(GUI_Element temp_element)
{
  String[] result_array = new String[3];
  ArrayList<Attribute> attributes_list = temp_element.getAttributeList();
  if(temp_element.isVisible())
  {
    for(int k = 0; k < attributes_list.size(); k++)
    {
      Attribute att = attributes_list.get(k);
      String att_name = att.getName();
      String att_value = att.getValue().toString();
      //println("property name: " + property_name);
      
      switch(att_name)
      {
        case "background_color":
        //println("background_color: " + value );
        result_array[0] = att_value;
        break;
      
        case "stroke_color":
        //println("stroke_color: " + stringToColor(value) );
        result_array[1] = att_value;
        break;
      
        case "stroke_weight":
        //println("stroke_weight: " + stringToColor(value) );
        result_array[2] = att_value;
        break;
    
      }
    }
  }
  return result_array;
}

public void drawText(GUI_Element temp_element)
{
  ArrayList<Attribute> attributes_list = temp_element.getAttributeList();
  PFont f;
  int font_index = -1;
  String font_color = "";
  String text = "";
  int text_alignment = -1;

  for(int k = 0; k < attributes_list.size(); k++)
  {
    Attribute att = attributes_list.get(k);
    String att_name = att.getName();
    String att_value = att.getValue().toString();
    
    /*
    program_title.addAttribute(new Attribute("font_type", _INDEX_FONT_OVERPASS_BOLD));
    program_title.addAttribute(new Attribute("font_color", _blue_stroke));
    program_title.addAttribute(new Attribute("text", _title_version));
    */
    
    switch(att_name)
    {
      case "font_type":
        font_index = PApplet.parseInt(att_value);
        break;
    
      case "font_color":
        font_color = att_value;
        break;

      case "text_alignment":
        text_alignment = PApplet.parseInt(att_value);
        
        break;
    
      case "text":
        text = att_value;
        break;
  
    }
  }

  f = _font_array.get( font_index );
  textFont(f);
  fill(stringToColor(font_color));
  textAlign(text_alignment);

  //textWidth()
  text(text, temp_element.getCurrentX(), temp_element.getCurrentY());
  //image(_blue_asterisk, mp_width - 154, mp_y0-27);
}

//font index, font color, text alignment, text
public String[] getTextProperties(GUI_Element temp_element) 
{
  String[] response = new String[4];
  
  ArrayList<Attribute> attributes_list = temp_element.getAttributeList();
 
  for(int k = 0; k < attributes_list.size(); k++)
  {
    Attribute att = attributes_list.get(k);
    String att_name = att.getName();
    String att_value = att.getValue().toString();
    
    /*
    program_title.addAttribute(new Attribute("font_type", _INDEX_FONT_OVERPASS_BOLD));
    program_title.addAttribute(new Attribute("font_color", _blue_stroke));
    program_title.addAttribute(new Attribute("text", _title_version));
    */
    
    switch(att_name)
    {
      case "font_type":
        response[0] = att_value;
        break;
    
      case "font_color":
        response[1] = att_value;
        break;

      case "text_alignment":
        response[2] = att_value;
        
        break;
    
      case "text":
        response[3] = att_value;
        break;
  
    }
  }
 
  return response;
}

public void drawIcon(GUI_Element temp_element)
{
  ArrayList<Attribute> attributes_list = temp_element.getAttributeList();
  int icon_index = -1;
  int x = temp_element.getX();
  int y = temp_element.getY();

  for(int k = 0; k < attributes_list.size(); k++)
  {
    Attribute att = attributes_list.get(k);
    String att_name = att.getName();
    String att_value = att.getValue().toString();
    
    switch(att_name)
    {
      case "icon_index":
        icon_index = PApplet.parseInt(att_value);
        break;
    }
  }
  PImage img = _icon_array.get(icon_index);
  image(img, temp_element.getCurrentX(), temp_element.getCurrentY());
}

public String getIconProperties(GUI_Element temp_element)
{
  String result = "";
  ArrayList<Attribute> attributes_list = temp_element.getAttributeList();
  int icon_index = -1;
  int x = temp_element.getX();
  int y = temp_element.getY();

  for(int k = 0; k < attributes_list.size(); k++)
  {
    Attribute att = attributes_list.get(k);
    String att_name = att.getName();
    String att_value = att.getValue().toString();
    
    switch(att_name)
    {
      case "icon_index":
        result = att_value;
        break;
    }
  }
  return result;
}

public void drawSensingArea(GUI_Element temp_element)
{
  int x = temp_element.getCurrentX();
  int y = temp_element.getCurrentY();
  int the_width = temp_element.getWidth();
  int the_height = temp_element.getHeight();

  String name = temp_element.getName();
  if (overRec(x, y, the_width, the_height) && _mouse_clicked)
  {
    _mouse_clicked = false;//maybe we remove this later
    if(name == "search_btn")
    {
      println("search: " + txf1.getText());
      txf1.setText("");
    }
    else if(name == "new_folder")
    {
      println("new folder");
      
    }
    else if(name == "folder_left_filter")
    {
      println("left filter click");
    }
    else if(name == "folder_right_filter")
    {
      println("right filter click");
    }
    else if(name == "play_btn")
    {
      boolean status = toggle(temp_element.getParent().getActiveStatus());
      temp_element.getParent().setActiveStatus(status);
      
      if(status)
      {
        float pos = PApplet.parseFloat(_active_note_detail_array.get(_active_note_detail_array.size()-1).getPositionOffsetX());
        float bar_width = PApplet.parseFloat(_active_note_detail_array.get(_active_note_detail_array.size()-2).getWidth());
        float cue = (pos/bar_width)*_audio_file.duration();
        _audio_file.play(1, 0, 1, 0, cue);
      }
      else{
        _audio_file.stop();
      }
      
    }
    else if(name == "skip_forward")
    {
      _skip_forward_flag = true;
    }
    else if(name == "skip_backward")
    {
      _skip_backward_flag = true;
    }
    else if (name == "playbar_line") 
    {
      _audio_file.stop();

      GUI_Element playhead = _active_note_detail_array.get(_active_note_detail_array.size()-1);
      GUI_Element playbar = _active_note_detail_array.get(_active_note_detail_array.size()-2);

      float pos = clipValue(mouseX, 0, playbar.getX(), (playbar.getX() + playbar.getWidth()));
      pos = pos - playbar.getX();

      playhead.setPositionOffsetX(PApplet.parseInt(pos));
      
      
      float bar_width = PApplet.parseFloat(playbar.getWidth());
      float cue = (pos/bar_width)*_audio_file.duration();
      _frame_counter = PApplet.parseInt(cue*_frame_rate);
      println( "cue: " + cue);
      println( "_frame_counter: " + _frame_counter);
      _playback_past_position = -1;
      _active_note_detail_array.get(_active_note_detail_array.size()-9).setActiveStatus(true);
      _audio_file.play(1, 0, 1, 0, cue);
    }
  }

  if(_show_sensing_areas)
  {
    fill(255,0,0,125);
    stroke(255,0,0);
    rect(x, y,the_width, the_height);
  }
}

public int checkFolderStatus(GUI_Element temp_element)
{
  int num_notes = 0;
  ArrayList<GUI_Element> children_list = temp_element.getChildren();
  //Check the active status of the folder
  if(temp_element.getType() == "folder")
  {

    updateFolderNoteStatus(temp_element);

    for(int i = 4; i < children_list.size(); i++)
    {
      if(children_list.get(i).getType() == "note")
      {
        if(temp_element.getActiveStatus())
        {
          updateFolderNoteStatus(children_list.get(i));
          num_notes++;
        }
        
      }
    }
    
    
  }
  
  return num_notes;//returns the number of actives notes under a folder for offset purposes.

}

public void checkNoteStatus(GUI_Element temp_element)
{
  updateFolderNoteStatus(temp_element);
}

public void updateFolderNoteStatus(GUI_Element temp_element)
{
  ArrayList<GUI_Element> children_list = temp_element.getChildren();
  PFont myfont;
  /*
  panel: 0
  icon: 1
  name: 2
  timestamp: 3
  */
  if(temp_element.getType() == "folder" || temp_element.getType() == "note")
  {
    if(areaIsClicked(temp_element.getSensingArea()))
    {
      //temp_element.toggleActiveStatus();
      boolean status = toggle(temp_element.getActiveStatus());
      if(temp_element.getType() == "folder")
      {
        deselectFolders();
      }
      else if (temp_element.getType() == "note") 
      {
        deselectNotes();
        _load_active_note_elements = true;
      }
      
      temp_element.setActiveStatus(status);
      temp_element.setChildrenStatus(status);
    }
  }

  if(temp_element.getType() == "note" && temp_element.getActiveStatus())
  {
    
    _new_note_active_status = true;
    displaySelectedNote(temp_element);
  }
  
}

public void displaySelectedNote(GUI_Element e)
{
  if(_load_active_note_elements == true)
  {
    int x_offset = 25;
    Note active_note = e.getNote();
    println("name: " + active_note.getName());
    println("timestamp: " + active_note.getTimestamp());
    println("transcription: " + active_note.getTranscription());

    _audio_file = new SoundFile(this, active_note.getNoteFilepath());

    _myTxArea.setText(active_note.getTranscription());

    GUI_Element line1 = addLine(_list_panel, "list_view", _list_panel.getX(), _list_panel.getY(), _list_panel.getX() + _list_panel.getWidth(), _list_panel.getY(), _background_color, 1);
    GUI_Element line2 = addLine(_list_panel, "list_view", (_list_panel.getX() + _list_panel.getWidth()) - 1, _list_panel.getY(), (_list_panel.getX() + _list_panel.getWidth()) - 1, PApplet.parseInt(_list_panel.getY() - _list_panel.getHeight()*0.15f), _background_color, 1);
    GUI_Element note_detail = addPanel(_main_panel, "both", 0.281f, 1, "right", "top", 0, 0, _blue_stroke , _blue_stroke, 1);

    //Adding text on the note detail panel

    
    //GUI_Element parent, String view_type, String text, int alignment, String font_color, int font_index, int x, int y, int w, int h
    GUI_Element note_name =  addTextField(note_detail, "list_view", active_note.getName(), LEFT, _background_color, _INDEX_FONT_OVERPASS_BOLD, 
      PApplet.parseInt(note_detail.getX() + x_offset),
      PApplet.parseInt(note_detail.getY() + 40),
      PApplet.parseInt(textWidth(active_note.getName())),
      10);

    //GUI_Element parent, String view_type, String text, int alignment, String font_color, int font_index, int x, int y, int w, int h
    GUI_Element note_ts =  addTextField(note_detail, "list_view", active_note.getTimestamp(), LEFT, _background_color, _INDEX_FONT_OVERPASS_BOLD_SMALLER, 
      PApplet.parseInt(note_detail.getX() + x_offset),
      PApplet.parseInt(note_detail.getY() + 60),
      PApplet.parseInt(textWidth(active_note.getTimestamp())),
      10);

    //GUI_Element parent, String view_type, String text, int alignment, String font_color, int font_index, int x, int y, int w, int h
    GUI_Element transcription_label =  addTextField(note_detail, "list_view", "Transcription", LEFT, _background_color, _INDEX_FONT_OVERPASS_BOLD_SMALLER, 
      PApplet.parseInt(note_detail.getX() + x_offset),
      PApplet.parseInt(note_detail.getY() + 120),
      PApplet.parseInt(textWidth("Transcription")),
      10);

    GUI_Element transcription_box = addPanel(note_detail, "both", 0.85f, 0.3f, "center", "top", 0, 130, _blue_stroke , _background_color, 1);
    //GUI_Element parent, String view_type, String text, int alignment, String font_color, int font_index, int x, int y, int w, int h
   
    GUI_Element play_btn = addActiveIcon(note_detail, "both", _INDEX_ICON_WHITE_STOP_BTN, _INDEX_ICON_WHITE_PLAY_BTN, 0.12f, 0.07f, "center", "bottom", 8, -30, _blue_stroke, _blue_stroke, 0);
    GUI_Element play_sa = addSensingArea(play_btn,"play_btn", "both", 1, 1, "left", "top", 0, 0);

    GUI_Element skip_forward_btn = addIcon(play_btn, "both", _INDEX_ICON_WHITE_PLAY_SKIP_FORWARD, 1, 1, "left", "top", 60, 5, _blue_stroke, _blue_stroke, 0);
    GUI_Element skip_forward_sa = addSensingArea(skip_forward_btn,"skip_forward", "both", 1, 1, "left", "top", 0, 0);

    GUI_Element skip_backward_btn = addIcon(play_btn, "both", _INDEX_ICON_WHITE_PLAY_SKIP_BACKWARD, 1, 1, "left", "top", -80, 5, _blue_stroke, _blue_stroke, 0);
    GUI_Element skip_backward_sa = addSensingArea(skip_backward_btn,"skip_backward", "both", 1, 1, "left", "top", 0, 0);

    //drawing the playback

    GUI_Element playbar_line = addPanel(note_detail, "both", 0.85f, 0.005f, "left", "bottom", x_offset, 0 - 2*PApplet.parseInt(_folder_list.getHeight()*0.2f) , _background_color , _background_color, 1);
    GUI_Element playbar_sa = addSensingArea(playbar_line,"playbar_line", "both", 1, 10, "left", "center", 0, 0);
    
    GUI_Element playbar_head = addPanel(note_detail, "both", 0.025f, 0.04f, "left", "bottom", x_offset, 0 - 2*PApplet.parseInt(_folder_list.getHeight()*0.2f) , _background_color , _background_color, 1);
    playbar_head.setPositionOffsetY(PApplet.parseInt(playbar_head.getHeight()*0.5f));
    //addLine(note_detail, "list_view", playbar_line.getX(), playbar_line.getY() + int((_folder_list.getHeight()*0.2)*0.25), playbar_line.getX(), playbar_line.getY() - int((_folder_list.getHeight()*0.2)*0.25), _background_color, 5);

    _active_note_detail_array = new ArrayList<GUI_Element>();
    _active_note_detail_array.add(line1);
    _active_note_detail_array.add(line2);
    _active_note_detail_array.add(note_detail);
    _active_note_detail_array.add(note_name);
    _active_note_detail_array.add(note_ts);
    _active_note_detail_array.add(transcription_label);
    _active_note_detail_array.add(transcription_box);
    
    _active_note_detail_array.add(play_btn);
    _active_note_detail_array.add(play_sa);

    _active_note_detail_array.add(skip_forward_btn);
    _active_note_detail_array.add(skip_forward_sa);

    _active_note_detail_array.add(skip_backward_btn);
    _active_note_detail_array.add(skip_backward_sa);

    //leave these to be the last ones in the array
    _active_note_detail_array.add(playbar_sa);
    _active_note_detail_array.add(playbar_line);
    _active_note_detail_array.add(playbar_head);
    
    _myTxArea.setVisible(true);

    _load_active_note_elements = false;
    _load_inactive_note_elements = true;
    _frame_counter = 0;
  
  }
}

public void drawInactiveNoteDetail()
{
  //mess around with the array only once after a state change (active><inactive)
  if(_load_inactive_note_elements)
  {
    GUI_Element note_detail = addPanel(_main_panel, "both", 0.281f, 1, "right", "top", 0, 0, _background_color, _blue_stroke, 1);

    String t = "Select Voice Note";
    //GUI_Element parent, String view_type, String text, int alignment, String font_color, int font_index, int x, int y, int w, int h
    GUI_Element default_note_text =  addTextField(note_detail, "list_view", t, CENTER, _blue_stroke, _INDEX_FONT_OVERPASS_BOLD, 
      PApplet.parseInt(note_detail.getX()+ note_detail.getWidth()*0.5f),
      PApplet.parseInt(note_detail.getY()+ note_detail.getHeight()*0.5f),
      PApplet.parseInt(textWidth(t)),
      10);

    _active_note_detail_array = new ArrayList<GUI_Element>();
    _active_note_detail_array.add(note_detail);
    _active_note_detail_array.add(default_note_text);
    _myTxArea.setVisible(false);
    
    _load_active_note_elements = true;
    _load_inactive_note_elements =false;

  }
  
}


public void deselectFolders()
{
  for(int i = 0; i < _folder_note_array.size(); i++)
  {
    GUI_Element e = _folder_note_array.get(i);
    if(e.getType() == "folder")
    {
      e.setActiveStatus(false);
      e.setChildrenStatus(false);
    }
  }
}

public void deselectNotes()
{
  for(int i = 0; i < _folder_note_array.size(); i++)
  {
    GUI_Element e = _folder_note_array.get(i);
    if(e.getType() == "folder")
    {
      ArrayList<GUI_Element> children_list = e.getChildren();
      for(int j = 0; j < children_list.size(); j++)
      {
        if(children_list.get(j).getType() == "note")
        {
          children_list.get(j).setActiveStatus(false);
          children_list.get(j).setChildrenStatus(false);
        }
      }
    }
  }

  for(int n = 0; n < _new_notes_array.size(); n++)
  {
    GUI_Element e = _new_notes_array.get(n);
    if(e.getType() == "note")
    {
      e.setActiveStatus(false);
      e.setChildrenStatus(false);
      
    }
  }
}

//return the y_position_offset value for the rest of the folders
public void drawFolderPGraphics(GUI_Element temp_element)
{
  ArrayList<GUI_Element> children_list = temp_element.getChildren();
  PFont myfont;
  /*
  panel: 0
  icon: 1
  name: 2
  timestamp: 3
  */

  GUI_Element folder_panel = children_list.get(0);
  GUI_Element icon = children_list.get(1);
  GUI_Element folder_text_name = children_list.get(2);
  GUI_Element folder_text_ts = children_list.get(3);

  //background_color, stroke_color, stroke_weight:
  String[] panel_properties = getPanelProperties(folder_panel); 
  int icon_index = PApplet.parseInt(getIconProperties(icon));
  //font index, font color, text alignment, text
  String[] name_properties = getTextProperties(folder_text_name);
  String[] ts_properties = getTextProperties(folder_text_ts);

  PImage img = _icon_array.get(icon_index);
  
  //println("panel dimensions: " + folder_panel.getWidth() + " x " +  folder_panel.getHeight() );
  //pg = createGraphics(folder_panel.getWidth(), folder_panel.getHeight());
  myfont = _font_array.get(PApplet.parseInt(name_properties[0]));
  

  _folder_item_pg.beginDraw();
  //pg.textFont(myfont);
  //_folder_item_pg.background(stringToColor(panel_properties[0]));
  _folder_item_pg.fill(stringToColor(panel_properties[0]));
  _folder_item_pg.stroke(stringToColor( panel_properties[1]));
  _folder_item_pg.strokeWeight(1);
  _folder_item_pg.rect(folder_panel.getCurrentX(), folder_panel.getCurrentY(), folder_panel.getWidth(), folder_panel.getHeight());
  _folder_item_pg.fill(stringToColor(name_properties[1]));
  _folder_item_pg.textSize(18);
  _folder_item_pg.text(name_properties[3], folder_text_name.getCurrentX(), folder_text_name.getCurrentY() );
  _folder_item_pg.text(ts_properties[3], folder_text_ts.getCurrentX(), folder_text_ts.getCurrentY());
  _folder_item_pg.image(img, icon.getCurrentX(), icon.getCurrentY());

  _folder_item_pg.endDraw();

  //only display notes that are currently active!
  for(int i = 4; i < children_list.size(); i++)
  {
    if(children_list.get(i).getType() == "note")
    {
      if(temp_element.getActiveStatus())
      {
        drawNotePGraphics(children_list.get(i));
      }
    }
  }

}

public void drawNotePGraphics(GUI_Element temp_element)
{
  ArrayList<GUI_Element> children_list = temp_element.getChildren();
  PFont myfont;
  /*
  panel: 0
  icon: 1
  name: 2
  timestamp: 3
  */
  
  GUI_Element folder_panel = children_list.get(0);
  GUI_Element icon = children_list.get(1);
  GUI_Element folder_text_name = children_list.get(2);
  GUI_Element folder_text_ts = children_list.get(3);
  
  //background_color, stroke_color, stroke_weight:
  String[] panel_properties = getPanelProperties(folder_panel); 
  int icon_index = PApplet.parseInt(getIconProperties(icon));
  //font index, font color, text alignment, text
  String[] name_properties = getTextProperties(folder_text_name);
  String[] ts_properties = getTextProperties(folder_text_ts);
  //println("panel dimensions: " + folder_panel.getWidth() + " x " +  folder_panel.getHeight() );
  //pg = createGraphics(folder_panel.getWidth(), folder_panel.getHeight());
  PImage img = _icon_array.get(icon_index);
  myfont = _font_array.get(PApplet.parseInt(name_properties[0]));

  _folder_item_pg.beginDraw();
  //pg.textFont(myfont);
  //_folder_item_pg.background(stringToColor(panel_properties[0]));
  _folder_item_pg.fill(stringToColor(panel_properties[0]));
  _folder_item_pg.stroke(stringToColor( panel_properties[1]));
  _folder_item_pg.strokeWeight(1);
  _folder_item_pg.rect(folder_panel.getCurrentX(), folder_panel.getCurrentY(), folder_panel.getWidth(), folder_panel.getHeight());
  _folder_item_pg.fill(stringToColor(name_properties[1]));
  _folder_item_pg.textSize(18);
  _folder_item_pg.text(name_properties[3], folder_text_name.getCurrentX(), folder_text_name.getCurrentY() );
  _folder_item_pg.text(ts_properties[3], folder_text_ts.getCurrentX(), folder_text_ts.getCurrentY());
  _folder_item_pg.image(img, icon.getCurrentX(), icon.getCurrentY());
  _folder_item_pg.endDraw();

}

public void drawNewNotePGraphics(GUI_Element temp_element)
{
  ArrayList<GUI_Element> children_list = temp_element.getChildren();
  PFont myfont;
  /*
  panel: 0
  icon: 1
  name: 2
  timestamp: 3
  */
  
  GUI_Element folder_panel = children_list.get(0);
  GUI_Element icon = children_list.get(1);
  GUI_Element folder_text_name = children_list.get(2);
  GUI_Element folder_text_ts = children_list.get(3);
  
  //background_color, stroke_color, stroke_weight:
  String[] panel_properties = getPanelProperties(folder_panel); 
  int icon_index = PApplet.parseInt(getIconProperties(icon));
  //font index, font color, text alignment, text
  String[] name_properties = getTextProperties(folder_text_name);
  String[] ts_properties = getTextProperties(folder_text_ts);
  //println("panel dimensions: " + folder_panel.getWidth() + " x " +  folder_panel.getHeight() );
  //pg = createGraphics(folder_panel.getWidth(), folder_panel.getHeight());
  PImage img = _icon_array.get(icon_index);
  myfont = _font_array.get(PApplet.parseInt(name_properties[0]));

  _new_notes_item_pg.beginDraw();
  //pg.textFont(myfont);
  //_folder_item_pg.background(stringToColor(panel_properties[0]));
  _new_notes_item_pg.fill(stringToColor(panel_properties[0]));
  _new_notes_item_pg.stroke(stringToColor( panel_properties[1]));
  _new_notes_item_pg.strokeWeight(1);
  _new_notes_item_pg.rect(folder_panel.getCurrentX(), folder_panel.getCurrentY(), folder_panel.getWidth(), folder_panel.getHeight());
  _new_notes_item_pg.fill(stringToColor(name_properties[1]));
  _new_notes_item_pg.textSize(18);
  _new_notes_item_pg.text(name_properties[3], folder_text_name.getCurrentX(), folder_text_name.getCurrentY() );
  _new_notes_item_pg.text(ts_properties[3], folder_text_ts.getCurrentX(), folder_text_ts.getCurrentY());
  _new_notes_item_pg.image(img, icon.getCurrentX(), icon.getCurrentY());
  _new_notes_item_pg.endDraw();

}


public boolean areaIsClicked(GUI_Element temp_element)
{
  if(temp_element.getType() == "sensing_area")
  {
    int x = temp_element.getCurrentX();
    int y = temp_element.getCurrentY();
    int w = temp_element.getWidth();
    int h = temp_element.getHeight();

    if(overRec(x,y,w,h) && _mouse_clicked)
    {
      _mouse_clicked = false;
      return true;
    }
  }
  
  return false;
}

public boolean overRec(int x, int y, int width, int height)  {
  
  if (mouseX >= x && mouseX <= x+width && 
      mouseY >= y && mouseY <= y+height) {
    return true;
  } else {
    return false;
  }
}

public void mousePressed() {
  _mouse_pressed = true;
  _folderScroll.mousePressedRect();
  _newNoteScroll.mousePressedRect();
}

public void mouseReleased() {
  _mouse_pressed = false;
  _folderScroll.mouseReleasedRect();
  _newNoteScroll.mouseReleasedRect();

}

public void mouseClicked() {
  /*if (_mouse_clicked == true) {
    _mouse_clicked = false;
  } else {
    _mouse_clicked = true;
  }*/
  _mouse_clicked = true;
}

public void readDatabase(){
  /*
  String transcript = "Almost before we knew it, we had left the ground";
   db = new SQLite( this, _db );  // open database file
  _note_array = new ArrayList<Note>();
    if ( db.connect() )
    {
        db.query( "SELECT * FROM table_one" );
        
        while (db.next())
        {
            _note_array.add( new Note(db.getString("field_one"), transcript ));
            
        }
    }
    _flag = false;
    */
}
class Asset{
	
	String _type;
	Object _the_asset;
	ArrayList<Attribute> _attribute_array = new ArrayList<Attribute>();

	Asset(String type, Object the_asset)
	{
		_type = type;
		_the_asset = the_asset;
	}
	
	public void addAttribute(Attribute an_attribute)
  	{
    	_attribute_array.add(an_attribute);
  	}

  	public Object getAsset()
  	{
  		return _the_asset;
  	}

  	public ArrayList<Attribute> getAttributes()
  	{
  		return _attribute_array;
  	}
}
class Attribute
{
  String _name = "";
  Object _value = null;
  String _type = "both"; //active, inactive, both

  /* 
  Each index in the ArrayList has an [object, object] array. 
  The first index of the object array is always text type while the second index can be text, color, of pimage.
    
  */
  Attribute(String name, Object value)
  {
    _name = name;
    _value = value;
  }

  Attribute(String name, Object value, String type)
  {
    _name = name;
    _type = type;
    _value = value;
  }

  public void setType(String type)
  {
    _type = type;
  }

  public String getType()
  {
    return _type;
  }

  public String getName()
  {
    return _name;
  }

  public Object getValue()
  {
    return _value;
  }

}
// VoiceNotes prototype
// Written by Wolfgang Gil
// info@thehonktweet.com

class Folder {

  int _id;
  String _name;
  String _timestamp;
  ArrayList<Note> _note_array;
  StringList _note_tags;
  
  Folder(int id, String name, String ts)
  {
    _id = id;
    _name = name;
    _note_array = new ArrayList<Note>();
    _timestamp = ts;
  }
  
  public void addNote(Note aNote)
  {
    _note_array.add(aNote);
  }
  
  public void addNotes(ArrayList<Note> notes)
  {
    _note_array = notes;
  }

  public ArrayList<Note> getNotes()
  {
    return _note_array;
  }
  
  public void setName(String name)
  {
    _name = name;  
  }
  
  public String getName()
  {
    return _name;
  }
  
  public String getTimeStamp()
  {
    return _timestamp;
  }
  
  public void updateDatabase(){
  }
}
class GUI_Element{
  
  GUI_Element _parent_element = null;
  GUI_Element _sensing_area = null;
  ArrayList<GUI_Element> _children_list = new ArrayList<GUI_Element>();

  String _name = "gui_element";

  boolean _is_interactive = false;
  boolean _is_active = false;
  boolean _is_visible = true;
  String _view_mode = "both"; //list_view, map_view
  String _type = ""; //ex: is the gui element a voice note? what are the types of elements?
  
  int _x = 0; //location of top left corner
  int _y = 0;
  int _width = 0;
  int _height = 0;

  int _scroll_y = 0;

  int _position_offset_x = 0;
  int _position_offset_y = 0;
  
  ArrayList<Attribute> _attribute_list = new ArrayList<Attribute>();
  Folder _folder = null;
  Note _note = null;

  GUI_Element(Folder a_folder)
  {
    _view_mode = "list_view";
    _folder = a_folder;
    _type = "folder";
    _is_interactive = true;
    _is_active = false;
  }

  GUI_Element(Note note)
  {
    _view_mode = "list_view";
    _note = note;
    _type = "note";
    _is_interactive = true;
    _is_active = false;
  }

  GUI_Element(String type, String view_mode, int x, int y, int w, int h)
  {
    _x = x;
    _y = y;
    _width = w;
    _height = h;
    _type = type;
    _view_mode = view_mode;
  }

  GUI_Element(String type, String view_mode, boolean is_interactive, int x, int y, int w, int h)
  {
    _x = x;
    _y = y;
    _width = w;
    _height = h;
    _type = type;
    _view_mode = view_mode;
    _is_interactive = is_interactive;
  }

  GUI_Element(String type, String view_mode)
  {
    _type = type;
    _view_mode = view_mode;
  }

  public void setName(String name)
  {
    _name = name;
  }

  public String getName()
  {
    return _name;
  }

  public void setSensingArea(GUI_Element sensing_area)
  {
    _sensing_area = sensing_area;
  }

  public GUI_Element getSensingArea()
  {
    return _sensing_area;
  }

  public void setInteractiveStatus(boolean i)
  {
    _is_interactive = i;
  }

  public void setActiveStatus(boolean a)
  {
    _is_active = a;
    //setChildrenStatus(a);
  }

  public boolean getActiveStatus()
  {
    return _is_active;
  }

  public void setVisibleStatus(boolean status)
  {
    _is_visible = status;
  }

  public boolean isVisible()
  {
    return _is_visible;
  }

  public boolean toggleActiveStatus()
  {
    if(_is_active == false)
    {
      _is_active = true;
      setChildrenStatus(true);
    }
    else
    {
      _is_active = false;
      setChildrenStatus(false);
    }

    return _is_active;
  }

  public void setChildrenStatus(boolean status)
  {
    for(int i = 0; i < _children_list.size(); i++)
    {
      if(_type == "folder" && _children_list.get(i).getType() == "note")
      {
        //do nothing
      }
      else 
      {
        _children_list.get(i).setActiveStatus(status);
      }
      
    }
  }
  public void addChildren(GUI_Element child)
  {
    _children_list.add(child);
    setChildrenStatus(_is_active);
  }

  public ArrayList<GUI_Element> getChildren()
  {
    return _children_list;
  }

  public boolean isInteractive()
  {
    return _is_interactive;
  }

  public void calculateAbsolutePosition()
  {
    //maybe there is a way to filter attributes by "position" type?

    //Default values 
    int x_offset = 10;
    int y_offset = 10;

    int parent_x = _parent_element.getX();
    int parent_y = _parent_element.getY();

    int parent_width = _parent_element.getWidth();
    int parent_height = _parent_element.getHeight();

    float parent_scale_x = 0.5f;
    float parent_scale_y = 0.5f;

    String horizontal_alignment = "left";
    String vertical_alignment = "top";
    int stroke_weight = 1;

    //now gather the actual values
    for(int i = 0; i<_attribute_list.size(); i++)
    {
      Attribute att = _attribute_list.get(i);
      String att_name = att.getName();
      String att_value = att.getValue().toString();
       
      switch(att_name){
        case "parent_scale_x":
          parent_scale_x = PApplet.parseFloat(att_value);
          break;

        case "parent_scale_y":
          parent_scale_y = PApplet.parseFloat(att_value);
          break;

        case "parent_align_x":
          horizontal_alignment = att_value;
          break;

        case "parent_align_y":
          vertical_alignment = att_value;
          break;

        case "parent_offset_x":
          x_offset = PApplet.parseInt(att_value);
          break;

        case "parent_offset_y":
          y_offset = PApplet.parseInt(att_value);
          break;

        case "stroke_weight":
          stroke_weight = PApplet.parseInt(att_value);
          break;
      }
    }

    //now do your calculatios!
    _width = PApplet.parseInt(parent_width * parent_scale_x);
    _height = PApplet.parseInt(parent_height * parent_scale_y);

    if(horizontal_alignment == "center")
    {
      _x = PApplet.parseInt( ((parent_width - _width)*0.5f) + parent_x );
    }
    else if(horizontal_alignment == "left")
    {
      _x = parent_x;
    }
    else
    {
      _x = ((parent_x + parent_width) - _width) ;
    }

    if(vertical_alignment == "center")
    {
      _y = PApplet.parseInt( ((parent_height - _height)*0.5f) + parent_y );
    }
    else if(vertical_alignment == "top")
    {
      _y = parent_y;
    }
    else
    {
      _y = ((parent_y + parent_height) - _height);
    }    
    
    _x += x_offset;
    _y += y_offset;
  }

  public void setPositionOffset(int x, int y)
  {
    _position_offset_x = x;
    _position_offset_y = y;
  }

  public void setChildrenPositionOffset(int x, int y)
  {
    if(_type == "folder" || _type == "note")
    {
      _sensing_area.setPositionOffset(x,y);
    }
    for(int i = 0; i < _children_list.size(); i++)
    {
      if(_children_list.get(i).getType() == "note")
      {
        _children_list.get(i).setChildrenPositionOffset(x,y);
      }
      else 
      {
        _children_list.get(i).setPositionOffset(x,y);
      }
      
    }
  }

  public void setChildrenScrollY(int y)
  {
    if(_type == "folder" || _type == "note")
    {
      _sensing_area.setScrollY(y);
    }
    for(int i = 0; i < _children_list.size(); i++)
    {
      if(_children_list.get(i).getType() == "note")
      {
        _children_list.get(i).setChildrenScrollY(y);
      }
      else 
      {
        _children_list.get(i).setScrollY(y);
      }
      
    }
  }

  public void setParentElement(GUI_Element parent)
  {
    _parent_element = parent;
  }
  
  public void addAttribute(Attribute an_attribute)
  {
    _attribute_list.add(an_attribute);
  }

  public void setWidth(int w)
  {
    _width = w;
  }

  public void setHeight(int h)
  {
    _height = h;
  }
  public void setPositionOffsetX(int x)
  {
    _position_offset_x = x;
  }
  public void setPositionOffsetY(int y)
  {
    _position_offset_y = y;
  }

  public void setScrollY(int y)
  {
    _scroll_y = y;
  }

  public void setX(int x)
  {
    _x = x;
  }
  public void setY(int y)
  {
    _y = y;
  }
  public GUI_Element getParent()
  {
    return _parent_element;
  }
  public int getPositionOffsetX()
  {
    return _position_offset_x;
  }

  public int getPositionOffsetY()
  {
    return _position_offset_y;
  }

  public int getCurrentX()
  {
    return _x + _position_offset_x;
  }

  public int getCurrentY()
  {
    return _y + _position_offset_y + _scroll_y;
  }

  public Note getNote()
  {
    return _note;
  }
  public String getViewMode()
  {
    return _view_mode;
  }
  
  public String getType()
  {
    return _type;
  }
  
  public int getX()
  {
    return _x;
  }
  
  public int getY()
  {
    return _y;
  }
  
  public int getWidth()
  {
    return _width;
  }
  
  public int getHeight()
  {
    return _height;
  }
  
  public ArrayList<Attribute> getAllAttributeList()
  {
    return _attribute_list;
  }

  public ArrayList<Attribute> getAttributeList()
  {
    if(_is_interactive)
    {
      if(_is_active) return getActiveAttributes();
      else return getInactiveAttributes();
    }
    else return getAllAttributeList();
    
  }

  public ArrayList<Attribute> getActiveAttributes()
  {
    ArrayList<Attribute> active_attributes = new ArrayList<Attribute>();
    for(int i = 0; i < _attribute_list.size(); i++)
    {
      Attribute att = _attribute_list.get(i);
      if(att.getType() == "active" || att.getType() == "both")
      {
        active_attributes.add(att);
      }
    }
    
    return active_attributes;
  }

  public ArrayList<Attribute> getInactiveAttributes()
  {
    ArrayList<Attribute> inactive_attributes = new ArrayList<Attribute>();
    for(int i = 0; i < _attribute_list.size(); i++)
    {
      Attribute att = _attribute_list.get(i);
      if(att.getType() == "inactive" || att.getType() == "both")
      {
        inactive_attributes.add(att);
      }
    }
    
    return inactive_attributes;
  }
  
  /*
  ArrayList<GUI_Element> getSubElements()
  {
    return _sub_elements_array;
  }*/
}
// Note class
// Written by Wolfgang Gil

class Note {

  int _id;
  String _name;
  String _timestamp;
  String _transcription;
  String _note_filepath;

  StringList _note_tags;
  
  Note(int id, String name, String transcription, String note_filepath, String timestamp)
  {
    _id = id;
    _name = name;
    _transcription = transcription;
    _timestamp = timestamp;
    _note_filepath = note_filepath;
  }
  
  public String getName()
  {
    return _name;
  }
  
  public String getTranscription()
  {
    return _transcription;
  }
  
  public String getTimestamp()
  {
    return _timestamp;
  }

  public String getNoteFilepath()
  {
    return _note_filepath;
  }
  
}
class ScrollRect {
 
  float rectPosX=0;
  float rectPosY=0;
  float rectWidth=14; 
  float rectHeight=30;
  
  float _var_x = 0;
  float _var_y = 0;

  float _var_height = 100;

  float max_val = 1000;
 
  boolean holdScrollRect=false; 
 
  float offsetMouseY; 
  boolean _is_visible = true;
 
  //constr
  ScrollRect(int var_x, int var_y, int var_height) {

    // you have to make a scrollRect in setup after size()
    rectPosX= var_x;
    rectPosY = var_y;
    _var_x = var_x;
    _var_y = var_y;
    _var_height = var_height;
  }//constr

  public void setVisibleStatus(boolean status)
  {
    _is_visible = status;
  }

  public boolean getVisibleStatus()
  {
    return _is_visible;
  }
 
  public void setWidth(int w)
  {
    rectWidth = w;
  }
  
  public void setHeight(int h)
  {
    rectHeight = h;
  }
  public void display() {
    fill(122);
    stroke(0);
    line (_var_x + rectWidth*0.5f, _var_y, _var_x + rectWidth*0.5f, _var_y+ _var_height);
    rect(rectPosX, rectPosY, 
      rectWidth, rectHeight);
 
    // Three small lines in the center   
    centerLine(-3); 
    centerLine(0);
    centerLine(3);
  }
 
  public void centerLine(float offset) {
    line(rectPosX+3, rectPosY+rectHeight/2+offset, 
      rectPosX+rectWidth-3, rectPosY+rectHeight/2+offset);
  }
 
  public void mousePressedRect() {
    if (mouseOver()) {
      holdScrollRect=true;
      offsetMouseY=mouseY-rectPosY;
    }
  }
 
  public void mouseReleasedRect() {
    holdScrollRect=false;
  }
 
  public void update() {
    // dragging of the mouse 
    if (holdScrollRect) {
      rectPosY=mouseY-offsetMouseY;
      if (rectPosY<_var_y)
        rectPosY=_var_y;
      if (rectPosY+rectHeight >(_var_y + _var_height)-1)
        rectPosY=(_var_y + _var_height)-rectHeight;
    }
  }
 
  public float scrollValue() {
    return map(rectPosY, _var_y , (_var_y + _var_height)-rectHeight, 0, 1000) * 0.001f;
  }
 
  public boolean mouseOver() {
    return mouseX>rectPosX&&
      mouseX<rectPosX+rectWidth&&
      mouseY>rectPosY&&
      mouseY<rectPosY+rectHeight;
  }//function 
  //
}//class 
//
  public void settings() {  size(1366, 768, P2D);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Voice_Notes_v1_01" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
