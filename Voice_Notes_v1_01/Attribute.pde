class Attribute
{
  String _name = "";
  Object _value = null;
  String _type = "both"; //active, inactive, both

  /* 
  Each index in the ArrayList has an [object, object] array. 
  The first index of the object array is always text type while the second index can be text, color, of pimage.
    
  */
  Attribute(String name, Object value)
  {
    _name = name;
    _value = value;
  }

  Attribute(String name, Object value, String type)
  {
    _name = name;
    _type = type;
    _value = value;
  }

  void setType(String type)
  {
    _type = type;
  }

  String getType()
  {
    return _type;
  }

  String getName()
  {
    return _name;
  }

  Object getValue()
  {
    return _value;
  }

}
