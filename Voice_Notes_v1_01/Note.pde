// Note class
// Written by Wolfgang Gil

class Note {

  int _id;
  String _name;
  String _timestamp;
  String _transcription;
  String _note_filepath;

  StringList _note_tags;
  
  Note(int id, String name, String transcription, String note_filepath, String timestamp)
  {
    _id = id;
    _name = name;
    _transcription = transcription;
    _timestamp = timestamp;
    _note_filepath = note_filepath;
  }
  
  String getName()
  {
    return _name;
  }
  
  String getTranscription()
  {
    return _transcription;
  }
  
  String getTimestamp()
  {
    return _timestamp;
  }

  String getNoteFilepath()
  {
    return _note_filepath;
  }
  
}
