class Asset{
	
	String _type;
	Object _the_asset;
	ArrayList<Attribute> _attribute_array = new ArrayList<Attribute>();

	Asset(String type, Object the_asset)
	{
		_type = type;
		_the_asset = the_asset;
	}
	
	void addAttribute(Attribute an_attribute)
  	{
    	_attribute_array.add(an_attribute);
  	}

  	Object getAsset()
  	{
  		return _the_asset;
  	}

  	ArrayList<Attribute> getAttributes()
  	{
  		return _attribute_array;
  	}
}