class GUI_Attribute
{
  int _x_offset = 0;
  int _y_offset = 0;
  
  String _type; //icon, text, geometry (maybe a box or a circle)
  ArrayList _attribute_array;
  
  //Example: 
  /* 
  Attribute type: text
  properties
  text: "this is an example"
  font_type: Overpass mono bold (active), Overpass mono regular (inactive)
  font_color: black (active), white (inactive)
  font_size: 12px
    
  */
  GUI_Attribute(String type)
  {
    _type = type;
  }
  
  void addProperties(String name, String[] values)
  {
    ArrayList temp_array = new ArrayList();
    
    temp_array.add(name);
    temp_array.add(values);
    
    _attribute_array.add(temp_array);
  }
  
  ArrayList getProperties()
  {
    return _attribute_array;
  }
}
