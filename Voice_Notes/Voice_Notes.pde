/*
  The Honk-Tweet
  Code by Wolfgang Gil, based on 
  
  The Nature of Code
  <http://www.shiffman.net/teaching/nature>
  Box2DProcessing example
  
  Library used: Box2D
  to import:
  1) click on Sketch -> import library -> add library
  2) in the search bar type box2d
  3) install "Box2D for Processing" by Daniel Shiffman
  
  ENJOY!
  
*/

color _background_color = #F5FFC7;
color _blue_stroke = #002EE8;
String _title_version = "VoiceNotes_v.11";
/****
 *  change the following two fields accordingly
 ****/
String _project_folder = "/Users/wolfganggil/voicenotes/";
String _asset_folder = "/Users/wolfganggil/voicenotes/Assets/";

PImage _decision_box;
PImage _search_bar;
PImage _new_folder_btn;
PImage _white_circle;
PImage _blue_circle;
PImage _white_lines;
PImage _blue_lines;
PImage _white_btn;
PImage _blue_asterisk;

boolean _list_view_mode = true;

PFont _overpass_bold;
PFont _overpass_light;
PFont _overpass_regular;

void setup() {
  
  size(1366, 768);
  smooth();
  
  loadIcons();
  loadFonts();
  
}

void loadIcons()
{
  _decision_box = loadImage(_asset_folder + "Icons/" + "white_decision_box.png");
  _search_bar = loadImage(_asset_folder + "Icons/" + "search_bar.png");
  _new_folder_btn = loadImage(_asset_folder + "Icons/" +"new_folder_btn.png");
  
  _white_circle = loadImage(_asset_folder + "Icons/" +"white_circle.png");
  _blue_circle = loadImage(_asset_folder + "Icons/" +"blue_circle.png");
  _white_lines = loadImage(_asset_folder + "Icons/" +"white_lines.png");
  _blue_lines = loadImage(_asset_folder + "Icons/" +"blue_lines.png");
  _white_btn = loadImage(_asset_folder + "Icons/" +"white_boton.png");
  
  _blue_asterisk = loadImage(_asset_folder + "Icons/" +"blue_asterisk.png");
}

void loadFonts()
{
   _overpass_bold = createFont(_asset_folder + "Fonts/" + "OverpassMono-Bold.ttf", 18);
   _overpass_light = createFont(_asset_folder + "Fonts/" + "OverpassMono-Light.ttf", 18);
   _overpass_regular = createFont(_asset_folder + "Fonts/" + "OverpassMono-Regular.ttf", 18);
}

void drawUIComponents()
{
  textFont(_overpass_bold);
  //drawing main panel
  int mp_width = 1297;
  int mp_height = 678;
  
  int mp_x0 = int((width-mp_width)*0.5);
  int mp_y0 = int((height-mp_height)*0.5);
  
  fill(_background_color);
  stroke(_blue_stroke);
  strokeWeight(1);
  rect(mp_x0, mp_y0, mp_width, mp_height);
  
  //draw project title
  
  fill(_blue_stroke);
  textAlign(LEFT);
  text(_title_version, mp_width - 130, mp_y0-12);
  image(_blue_asterisk, mp_width - 154, mp_y0-27);

  //draw search bar panel
  int bb_width = 930;
  int bb_height = 90;
  
  fill(_blue_stroke);
  rect(mp_x0, mp_y0, bb_width, bb_height);
  
  //add decision box
   image(_decision_box, mp_x0+22, mp_y0+15);
   if(_list_view_mode)
   {
     //switch to list view
     image(_white_btn,  mp_x0+77, mp_y0+16);
     image(_white_circle,  mp_x0+35, mp_y0+29);
     image(_blue_lines,  mp_x0+93, mp_y0+35);
   }
   else
   {
     //switch to list view
     image(_white_btn,  mp_x0+22, mp_y0+16);
     image(_blue_circle,  mp_x0+38, mp_y0+31);
     image(_white_lines,  mp_x0+93, mp_y0+35);
   }
   
   //add search bar
   image(_search_bar, mp_x0+452, mp_y0+15);
   image(_new_folder_btn, mp_x0+160, mp_y0+16);
   
   //draw right panel
   int side_panel_width = mp_width - bb_width;
   int side_panel_height = mp_height;
   
   fill(_background_color);
   stroke(_blue_stroke);
   rect(mp_x0+bb_width,mp_y0, side_panel_width,side_panel_height);
   
   fill(_blue_stroke);
   textAlign(CENTER);
   text("Select Voice Note", mp_x0 + bb_width + (side_panel_width*0.5), mp_y0 + (side_panel_height*0.5));
   
}

void draw() {
  
  background(_background_color);
  drawUIComponents();
   // We must always step through time!

  if (mousePressed) {
    //if(overRec(int x, int y, int width, int height))
  }
  else
  {
       
  }
 
}

boolean toggle(boolean val)
{
  if(val) return false;
  else return true;
}

boolean overRec(int x, int y, int width, int height)  {
  
  if (mouseX >= x && mouseX <= x+width && 
      mouseY >= y && mouseY <= y+height) {
    return true;
  } else {
    return false;
  }
}

void readDatabase(){
  /*
  String transcript = "Almost before we knew it, we had left the ground";
   db = new SQLite( this, _db );  // open database file
  _note_array = new ArrayList<Note>();
    if ( db.connect() )
    {
        db.query( "SELECT * FROM table_one" );
        
        while (db.next())
        {
            _note_array.add( new Note(db.getString("field_one"), transcript ));
            
        }
    }
    _flag = false;
    */
}
