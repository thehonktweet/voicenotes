// Note class
// Written by Wolfgang Gil

class Note {

  String _name;
  String _timestamp;
  String _transcription;
  StringList _note_tags;
  
  Note(String name, String transcription, String timestamp)
  {
    _name = name;
    _transcription = transcription;
    _timestamp = timestamp;
  }
  
  String getName()
  {
    return _name;
  }
  
  String getTranscription()
  {
    return _transcription;
  }
  
  String getTimestamp()
  {
    return _timestamp;
  }
  
}
