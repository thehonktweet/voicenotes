class GUI_Element{
  
  String _name;
  String _view_mode;
  String _type = ""; //ex: is the gui element a voice note?
  
  int _x; //location of top left corner
  int _y;
  int _width;
  int _height;
  
  ArrayList<GUI_Element> _sub_elements_array;
  ArrayList<GUI_Attribute> _attribute_array;
  
  GUI_Element(String name, int x, int y, int w, int h)
  {
    _x = x;
    _y = y;
    _width = w;
    _height = h;
    _name = name; 
  }
  
  void setSubElements(ArrayList<GUI_Element> se)
  {
    _sub_elements_array = se;
  }
}
