// VoiceNotes prototype
// Written by Wolfgang Gil
// info@thehonktweet.com

class Folder {

  String _name;
  String _timestamp;
  ArrayList<Note> _note_array;
  StringList _note_tags;
  
  Folder(String name, String ts)
  {
    _name = name;
    _note_array = new ArrayList<Note>();
    _timestamp = ts;
  }
  
  void addNote(Note aNote)
  {
    _note_array.add(aNote);
  }
  
  void addNotes(ArrayList<Note> notes)
  {
    _note_array = notes;
  }
  
  void setName(String name)
  {
    _name = name;  
  }
  
  String getName()
  {
    return _name;
  }
  
  String getTimeStamp()
  {
    return _timestamp;
  }
}
